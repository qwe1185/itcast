package com.itheima.dao.store;

import com.itheima.domain.store.City;

import java.util.List;

public interface CityDao {
    List<City> findAll();
}
