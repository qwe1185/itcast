package com.itheima.dao.store;

import com.itheima.domain.store.Orders;

import java.util.List;

public interface OrdersDao {
    public int deleteById(String[] strings);

    public int deleteOTById(String[] strings);

    public List<Orders> selectAll();

    public Orders selectAllById(String id);

    public int updateOpen(String[] strings);

    public int updateClose(String[] strings);

    public List<Orders> fuzzy(String desc);
}
