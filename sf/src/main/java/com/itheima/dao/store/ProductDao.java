package com.itheima.dao.store;

import com.itheima.domain.store.Product;

import java.util.List;

public interface ProductDao {
    int save(Product product);

    int delete(String[] ids);

    int update(Product product);

    Product findById(String id);

    List<Product> findAll();

    int changeStatus(String[] id);

    int openStatus(String[] id);

    List<Product> findByLike(String productNum);

}
