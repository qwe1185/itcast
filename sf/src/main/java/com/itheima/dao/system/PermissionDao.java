package com.itheima.dao.system;

import com.itheima.domain.store.Orders;
import com.itheima.domain.system.Permission;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PermissionDao {

    int save(Permission permission);
    int delete(Permission permission);
    int update(Permission permission);

    Permission findById(String id);

    List<Permission> findAll();

    List<Map> findAuthorDataByRoleId(String roleId);

    //用户登录查询对应用户所有的
    List<Permission> findPermissionByUserId(String id);

    //复选框多选删除
     int deleteOTById(String[] strings);

     //模糊查询
     public List<Permission> fuzzy(@Param("desc") String desc);
}
