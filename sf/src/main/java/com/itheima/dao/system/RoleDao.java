package com.itheima.dao.system;


import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleDao {

    int save(Role role);

    int delete(String[] ids);

    int update(Role role);

    Role findById(String id);

    List<Role> findAll();


    PageInfo findAll(int page, int size);

    void deleteRolePermission(String roleId);

    void saveRolePermission(@Param("roleId") String roleId, @Param("permissionId") String permissionId);

    List<Role> findAllRoleByUserId(String userId);

    List<Role> fuzzy(String rn);

}
