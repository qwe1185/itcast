package com.itheima.dao.system;
import com.itheima.domain.system.SysLog;
import java.util.List;
public interface SysLogDao {
    //存储信息
    void save(SysLog sysLog);

    //删除信息
    void delete(String[] ids);

    //查询所有信息
    List<SysLog> finds(String time);

    //查
    List<SysLog> findAll();
    //下载导出

}
