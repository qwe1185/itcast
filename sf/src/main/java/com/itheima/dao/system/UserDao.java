package com.itheima.dao.system;

import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserDao {

    //登陆查询用户的sql
    @Select("select * from users where username =#{username} and password=#{password}")
    User findByUsernameAndPwd(@Param("username") String username, @Param("password") String pwd);

    //修改密码的方法
    @Update("update users set password = #{password} where id= #{id}")
    void updatePwd(@Param("id") String id, @Param("password") String pwd);

    //根据邮箱查询用户的方法
    @Select("select * from users where email = #{email}")
    User findUserByNameOrEmail(String email);

    @Update("update users set password =#{password} where username =#{username}")
    void UpdateByName(@Param("username") String userName, @Param("password") String newPassword);

    int save(User user);

    int delete(User user);

    int update(User user);

    User findById(String id);

    List<User> findAll();

    void deleteSel(String[] ids);

    List<User> findAllRoleByUserId(String userId);

    void updateRole(@Param("userId") String userId, @Param("roleId") String roleId);

    void updateStatus(User user);

    List<User> findByName(String name);

    void deleteRole(String userId);

    User findByEmail(String email);
}
