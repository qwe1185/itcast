package com.itheima.factory;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DaoInstanceFactory {

    private static SqlSessionFactory factory = null;

    static {
        try {
            InputStream is = Resources.getResourceAsStream("MyBatisConfig.xml");

            factory = new SqlSessionFactoryBuilder().build(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> T getMapper(Class<T> daoClass) {
        //1. 获取 SqlSession
        SqlSession sqlSession = factory.openSession();

        // 2. 调用框架生成到代理对象并返回

        // 真实对象
        T t = sqlSession.getMapper(daoClass);

        // 动态代理
        T proxy = (T) Proxy.newProxyInstance(daoClass.getClassLoader(), new Class[]{daoClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                Object value = null;
                try {
                    value = method.invoke(t, args); // dao.save(user)
                    // 成功: 提交事务
                    sqlSession.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                    sqlSession.rollback();
                } finally {
                    sqlSession.close();
                }
                return value;
            }
        });

        return proxy;
    }


}
