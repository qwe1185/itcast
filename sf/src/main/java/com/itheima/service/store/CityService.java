package com.itheima.service.store;

import com.itheima.domain.store.City;

import java.util.List;

public interface CityService {
    List<City> findAll();
}
