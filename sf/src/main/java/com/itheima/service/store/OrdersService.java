package com.itheima.service.store;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.store.Orders;

import java.util.List;

public interface OrdersService {
    public void deleteById(String[] strings);

    public void deleteOTById(String[] strings);

    public PageInfo selectAll(int page, int size);

    public Orders selectAllById(String id);

    public void updateOpen(String[] strings);

    public void updateClose(String[] strings);

    public PageInfo fuzzy(int page, int size, String desc);
}
