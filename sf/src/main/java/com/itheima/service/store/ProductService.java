package com.itheima.service.store;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.store.Product;

import java.util.List;

public interface ProductService {
    /**
     * 添加
     *
     * @param product
     * @return
     */
    void save(Product product);

    /**
     * 删除
     *
     * @param
     * @return
     */
    void delete(String[] ids);

    /**
     * 修改
     *
     * @param product
     * @return
     */
    void update(Product product);

    /**
     * 查询单个
     *
     * @param id 查询的条件（id）
     * @return 查询的结果，单个对象
     */
    Product findById(String id);

    /**
     * 查询全部的数据
     *
     * @return 全部数据的列表对象
     */
    List<Product> findAll();

    /**
     * 分页查询数据
     *
     * @param page 页码
     * @param size 每页显示的数据总量
     * @return
     */
    PageInfo findByPage(int page, int size);

    void changeStatus(String[] id, String name);

    List<Product> findByLike(String productNum);
}
