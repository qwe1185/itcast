package com.itheima.service.store.impl;

import com.itheima.dao.store.CityDao;
import com.itheima.domain.store.City;
import com.itheima.factory.DaoInstanceFactory;
import com.itheima.service.store.CityService;

import java.util.List;

public class CityServiceImpl implements CityService {
    @Override
    public List<City> findAll() {
        CityDao mapper = DaoInstanceFactory.getMapper(CityDao.class);
        return mapper.findAll();
    }
}
