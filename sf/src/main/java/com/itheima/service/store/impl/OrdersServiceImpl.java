package com.itheima.service.store.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.store.OrdersDao;
import com.itheima.domain.store.Orders;
import com.itheima.factory.DaoInstanceFactory;
import com.itheima.service.store.OrdersService;

import java.util.List;

public class OrdersServiceImpl implements OrdersService {
    @Override
    public void deleteById(String[] strings) {
        OrdersDao ordersDao = DaoInstanceFactory.getMapper(OrdersDao.class);
        ordersDao.deleteById(strings);
    }

    @Override
    public void deleteOTById(String[] strings) {
        OrdersDao ordersDao = DaoInstanceFactory.getMapper(OrdersDao.class);
        ordersDao.deleteOTById(strings);
    }

    @Override
    public PageInfo selectAll(int page, int size) {
        OrdersDao ordersDao = DaoInstanceFactory.getMapper(OrdersDao.class);
        PageHelper.startPage(page, size);
        List<Orders> orders = ordersDao.selectAll();
        return new PageInfo<>(orders);
    }

    @Override
    public Orders selectAllById(String id) {
        OrdersDao ordersDao = DaoInstanceFactory.getMapper(OrdersDao.class);
        return ordersDao.selectAllById(id);
    }

    @Override
    public void updateOpen(String[] strings) {
        OrdersDao ordersDao = DaoInstanceFactory.getMapper(OrdersDao.class);
        ordersDao.updateOpen(strings);
    }

    @Override
    public void updateClose(String[] strings) {
        OrdersDao ordersDao = DaoInstanceFactory.getMapper(OrdersDao.class);
        ordersDao.updateClose(strings);
    }

    @Override
    public PageInfo fuzzy(int page, int size, String desc) {
        OrdersDao ordersDao = DaoInstanceFactory.getMapper(OrdersDao.class);
        PageHelper.startPage(page, size);
        List<Orders> fuzzy = ordersDao.fuzzy(desc);
        return new PageInfo<>(fuzzy);
    }


}
