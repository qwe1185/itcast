package com.itheima.service.store.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.store.ProductDao;
import com.itheima.domain.store.Product;
import com.itheima.factory.DaoInstanceFactory;
import com.itheima.service.store.ProductService;
import com.itheima.utils.UUIDUtil;

import java.util.List;

public class ProductServiceImpl implements ProductService {
    @Override
    public List<Product> findAll() {
        ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
        return mapper.findAll();
    }

    @Override
    public Product findById(String id) {
        ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
        return mapper.findById(id);
    }

    @Override
    public void save(Product product) {
        String uuid = UUIDUtil.getId();
        product.setId(uuid);
        ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
        mapper.save(product);
    }

    @Override
    public void delete(String[] ids) {
        ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
        mapper.delete(ids);
    }

    @Override
    public void update(Product product) {
        ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
        mapper.update(product);
    }

    @Override
    public PageInfo<Product> findByPage(int currentPage, int pageSize) {
        PageHelper.startPage(currentPage, pageSize);
        ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
        List<Product> list = mapper.findAll();
        PageInfo<Product> info = new PageInfo<>(list);
        return info;
    }

    @Override
    public void changeStatus(String[] id,String name) {
        if("shield".equals(name)){
            ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
            mapper.changeStatus(id);
        }else if("open".equals(name)){
            ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
            mapper.openStatus(id);
        }

    }

    @Override
    public List<Product> findByLike( String productNum) {

        ProductDao mapper = DaoInstanceFactory.getMapper(ProductDao.class);
        List<Product> list = mapper.findByLike(productNum);
        return list;
    }
}
