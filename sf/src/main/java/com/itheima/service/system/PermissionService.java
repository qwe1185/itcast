package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Permission;

import java.util.List;
import java.util.Map;

public interface PermissionService {

    void save(Permission permission);
    void delete(Permission permission);
    void update(Permission permission);

    Permission findById(String id);

    List<Permission> findAll();

    PageInfo findAll(int page, int size);

    /**
     * 根据角色id获取对应的所有模块关联数据
     * @param roleId 角色id
     */
    List<Map> findAuthorDataByRoleId(String roleId);

    //多选删除
    void deleteOTById(String[] ids);

    PageInfo fuzzy(int page, int size, String desc);
}
