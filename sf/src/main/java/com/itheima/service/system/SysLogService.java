package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.SysLog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;

public interface SysLogService {
    //1.保存
    void save(SysLog sysLog);
    //2.删除
    void delete(String[] ids);
    //3.分页查询
    PageInfo<SysLog> findPage(int page, int size);

    //4.下载导出
    ByteArrayOutputStream getReport(String time) throws IOException;


    ByteArrayOutputStream getReport() throws IOException;
}
