package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Permission;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;

import java.util.List;

public interface UserService {
    User login(String username, String pwd);

    User aLogin(String username,String pwd);

    void updatePwd(String id,String pwd);

    void UpdateByName(String userName, String newPassword);

    List<Permission> findPermissionById(String id);

    void save(User user);

    void delete(User user);

    void update(User user);

    User findById(String id);

    List<User> findAll();

    /**
     * 分页
     * @param page 当前页 size 每页显示条数
     * @return PageInfo对象
     */
    PageInfo findAll(int page, int size);

    void deleteSel(String[] ids);

    List<User> findAllRoleByUserId(String userId);

    void updateRole(String userId, String[] roleIds);

    void updateStatus(User user);

    PageInfo findByName(String name, int page, int size);

    User findByEmail(String email);
}
