package com.itheima.service.system.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.store.OrdersDao;
import com.itheima.dao.system.PermissionDao;
import com.itheima.domain.store.Orders;
import com.itheima.domain.system.Permission;
import com.itheima.factory.DaoInstanceFactory;
import com.itheima.service.system.PermissionService;
import com.itheima.utils.UUIDUtil;

import java.util.List;
import java.util.Map;

public class PermissionServiceImpl implements PermissionService {
    @Override
    public void save(Permission permission) {
        PermissionDao mapper = DaoInstanceFactory.getMapper(PermissionDao.class);
        permission.setId(UUIDUtil.getId());
        mapper.save(permission);
    }

    @Override
    public void delete(Permission permission) {
        PermissionDao mapper = DaoInstanceFactory.getMapper(PermissionDao.class);
        mapper.delete(permission);

    }

    @Override
    public void update(Permission permission) {
        DaoInstanceFactory.getMapper(PermissionDao.class).update(permission);
    }

    @Override
    public Permission findById(String id) {
        return DaoInstanceFactory.getMapper(PermissionDao.class).findById(id);
    }

    @Override
    public List<Permission> findAll() {
        return DaoInstanceFactory.getMapper(PermissionDao.class).findAll();
    }

    @Override
    public PageInfo findAll(int page, int size) {
        PageHelper.startPage(page, size);
        PermissionDao mapper = DaoInstanceFactory.getMapper(PermissionDao.class);
        List<Permission> all = mapper.findAll();
        PageInfo pageInfo = new PageInfo(all);
        return pageInfo;
    }

    @Override
    public List<Map> findAuthorDataByRoleId(String roleId) {
        PermissionDao mapper = DaoInstanceFactory.getMapper(PermissionDao.class);
        return mapper.findAuthorDataByRoleId(roleId);
    }

    @Override
    public void deleteOTById(String[] ids) {
        PermissionDao mapper = DaoInstanceFactory.getMapper(PermissionDao.class);
        mapper.deleteOTById(ids);
    }

    @Override
    public PageInfo fuzzy(int page, int size, String desc) {
        PermissionDao mapper = DaoInstanceFactory.getMapper(PermissionDao.class);
        PageHelper.startPage(page, size);
        List<Permission> list = mapper.fuzzy(desc);
        PageInfo<Permission> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }


}
