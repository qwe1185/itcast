package com.itheima.service.system.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.store.OrdersDao;
import com.itheima.dao.system.RoleDao;
import com.itheima.domain.store.Orders;
import com.itheima.domain.system.Role;
import com.itheima.factory.DaoInstanceFactory;
import com.itheima.service.system.RoleService;
import com.itheima.utils.UUIDUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.UUID;

public class RoleServiceImpl implements RoleService {
    @Override
    public void save(Role role) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        String id = UUIDUtil.getId();
        role.setId(id);
        dao.save(role);
    }

    @Override
    public void delete(String[] ids) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        dao.delete(ids);

    }

    @Override
    public void update(Role role) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        dao.update(role);
    }

    @Override
    public Role findById(String id) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        return dao.findById(id);

    }

    @Override
    public List<Role> findAll() {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        return dao.findAll();
    }

    @Override
    public PageInfo findAll(int page, int size) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        PageHelper.startPage(page, size);
        List<Role> all = dao.findAll();
        PageInfo<Role> pageInfo = new PageInfo<>(all);
        return pageInfo;
    }

    @Override
    public void updateRolePermission(String roleId, String permissionIds) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        dao.deleteRolePermission(roleId);

        String[] permissionArray = permissionIds.split(",");
        for (String permissionId : permissionArray) {
            RoleDao dao1 = DaoInstanceFactory.getMapper(RoleDao.class);
            dao1.saveRolePermission(roleId, permissionId);
        }
    }

    @Override
    public List<Role> findAllRoleByUserId(String userId) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        return dao.findAllRoleByUserId(userId);
    }

    @Override
    public void deleteRolePermission(String roleId) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        dao.deleteRolePermission(roleId);

    }

    @Override
    public void saveRolePermission(String roleId, String permissionId) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        dao.saveRolePermission(roleId, permissionId);
    }


    @Override
    public PageInfo<Role> fuzzy(int page, int size, String rn) {
        RoleDao dao = DaoInstanceFactory.getMapper(RoleDao.class);
        PageHelper.startPage(page, size);

        List<Role> fuzzy = dao.fuzzy(rn);
        return new PageInfo<>(fuzzy);
    }


}

















