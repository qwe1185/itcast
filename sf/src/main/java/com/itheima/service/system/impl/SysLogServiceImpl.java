package com.itheima.service.system.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.system.SysLogDao;
import com.itheima.domain.system.SysLog;
import com.itheima.factory.DaoInstanceFactory;
import com.itheima.service.system.SysLogService;
import org.apache.ibatis.session.SqlSession;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SysLogServiceImpl implements SysLogService {
    private static SysLogDao dao = null;
    @Override
    public void save(SysLog sysLog) {
        SysLogDao dao = DaoInstanceFactory.getMapper(SysLogDao.class);
        dao.save(sysLog);

    }

    @Override
    public void delete(String[] ids) {
        SysLogDao sysLogDao = DaoInstanceFactory.getMapper(SysLogDao.class);
         sysLogDao.delete(ids);
    }

    @Override
    public PageInfo<SysLog> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        List<SysLog> sysLogs = DaoInstanceFactory.getMapper(SysLogDao.class).findAll();
        PageInfo<SysLog> info = new PageInfo<>(sysLogs);
        return info;
    }

    @Override
    public ByteArrayOutputStream getReport(String time) throws IOException {
        return null;
    }

    @Override
    public ByteArrayOutputStream getReport() throws IOException {
        SysLogDao sysLogDao = DaoInstanceFactory.getMapper(SysLogDao.class);
        List<SysLog> syslogList =sysLogDao.findAll();
        //获取工作簿文件
        Workbook wb = new XSSFWorkbook();
        //2创建工作表
        Sheet s = wb.createSheet("日志文件");

        //制作标题
        s.addMergedRegion(new CellRangeAddress(1, 1, 1, 7));

        Row row_1 = s.createRow(1);
        Cell cell_1_1 = row_1.createCell(1);
        cell_1_1.setCellValue("标题信息");

        CellStyle cs_title = wb.createCellStyle();
        cs_title.setAlignment(HorizontalAlignment.CENTER);
        cs_title.setVerticalAlignment(VerticalAlignment.CENTER);
        cell_1_1.setCellStyle(cs_title);

        //制作表头
        String Fileds[] = {"ID", "访问时间", "访问用户", "访问IP", "资源URL", "执行时间", "执行方法"};
        Row row_2 = s.createRow(2);
        for (int i = 0; i < Fileds.length; i++) {
            Cell cell_2_temp = row_2.createCell(1 + i);
            cell_2_temp.setCellValue(Fileds[i]);
            cell_2_temp.setCellStyle(cs_title);
        }
        //制作表区
        int row_index = 0;
        System.out.println(syslogList);
        for (SysLog q : syslogList) {
            int cell_index = 0;
            Row row_temp = s.createRow(3 + row_index++);

            Cell cell_data_1 = row_temp.createCell(1 + cell_index++);
            cell_data_1.setCellValue(q.getId());

            Cell cell_data_2 = row_temp.createCell(1 + cell_index++);
            cell_data_2.setCellValue(q.getVisitTime());

            Cell cell_data_3 = row_temp.createCell(1 + cell_index++);
            cell_data_3.setCellValue(q.getUsername());

            Cell cell_data_4 = row_temp.createCell(1 + cell_index++);
            cell_data_4.setCellValue(q.getIp());

            Cell cell_data_5 = row_temp.createCell(1 + cell_index++);
            cell_data_5.setCellValue(q.getUrl());

            Cell cell_data_6 = row_temp.createCell(1 + cell_index++);
            cell_data_6.setCellValue(q.getVisitTimeStr());

            Cell cell_data_7 = row_temp.createCell(1 + cell_index++);
            cell_data_7.setCellValue(q.getMethod());
        }

       ByteArrayOutputStream os = new ByteArrayOutputStream();
 /*        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setDataFormat(wb.createDataFormat().getFormat("yyyy年MM月dd日hh时mm分ss秒"));*/
        wb.write(os);
        wb.close();
        return os;
    }


}


