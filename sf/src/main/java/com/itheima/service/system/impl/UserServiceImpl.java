package com.itheima.service.system.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.itheima.dao.system.PermissionDao;
import com.itheima.dao.system.UserDao;
import com.itheima.domain.system.Permission;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;
import com.itheima.factory.DaoInstanceFactory;
import com.itheima.service.system.UserService;
import com.itheima.utils.MD5Util;
import com.itheima.utils.UUIDUtil;

import javax.sound.midi.Soundbank;
import java.util.List;

public class UserServiceImpl implements UserService {
    //登陆查询用户的方发
    @Override
    public User login(String username, String pwd) {
        //创建userDao对象到数据库查询对象
        UserDao userDao = DaoInstanceFactory.getMapper(UserDao.class);
        //密码加密
        pwd = MD5Util.md5(pwd);

        return userDao.findByUsernameAndPwd(username, pwd);
    }

    @Override
    public User aLogin(String username, String pwd) {
        UserDao userDao = DaoInstanceFactory.getMapper(UserDao.class);
        System.out.println(MD5Util.md5("123456"));
        System.out.println(pwd);
        return userDao.findByUsernameAndPwd(username, pwd);
    }


    //修改密码的方法
    @Override
    public void updatePwd(String id, String pwd) {
        //创建userDao对象到数据库查询对象
        UserDao userDao = DaoInstanceFactory.getMapper(UserDao.class);
        //密码加密
        pwd = MD5Util.md5(pwd);

        userDao.updatePwd(id, pwd);
    }

    //根据名字修改密码
    @Override
    public void UpdateByName(String userName, String newPassword) {

        UserDao userDao = DaoInstanceFactory.getMapper(UserDao.class);

        userDao.UpdateByName(userName, newPassword);
    }

    @Override
    public List<Permission> findPermissionById(String id) {
        PermissionDao permissionDao = DaoInstanceFactory.getMapper(PermissionDao.class);

        return permissionDao.findPermissionByUserId(id);
    }

    /**
     * 新建用户(保存)
     *
     * @param user
     */
    @Override
    public void save(User user) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        String id = UUIDUtil.getId();
        System.out.println(id);
        user.setId(id);
        user.setPassword(MD5Util.md5(user.getPassword()));
        dao.save(user);
    }

    /**
     * 删除用户
     *
     * @param user
     */
    @Override
    public void delete(User user) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        dao.delete(user);
    }

    /**
     * 删除用户
     *
     * @param user
     */
    @Override
    public void update(User user) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        dao.update(user);
    }

    /**
     * 通过id查询用户
     *
     * @param id
     */
    @Override
    public User findById(String id) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        User user = dao.findById(id);
        return user;
    }


    /**
     * 查询所有用户
     *
     * @param
     */
    @Override
    public List<User> findAll() {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        List<User> users = dao.findAll();
        return users;
    }

    @Override
    public PageInfo findAll(int page, int size) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        //3.调用dao层操作
        PageHelper.startPage(page, size);
        List<User> all = dao.findAll();
        for (int i = 0; i < all.size(); i++) {
            if (all.get(i).getStatus() == 0) {
                all.get(i).setStatusStr("关闭");
            } else {
                all.get(i).setStatusStr("开启");
            }
        }
        PageInfo<User> pageInfo = new PageInfo<>(all);
        return pageInfo;
    }

    @Override
    public void deleteSel(String[] ids) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        dao.deleteSel(ids);
    }

    @Override
    public List<User> findAllRoleByUserId(String userId) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        return dao.findAllRoleByUserId(userId);
    }

    @Override
    public void updateRole(String userId, String[] roleIds) {
        UserDao daoD = DaoInstanceFactory.getMapper(UserDao.class);
        daoD.deleteRole(userId);
        for (String roleId : roleIds) {
            UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
            dao.updateRole(userId, roleId);
        }
    }

    @Override
    public void updateStatus(User user) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        int status = user.getStatus();
        if (status == 0) {
            user.setStatus(1);
        } else {
            user.setStatus(0);
        }
        dao.updateStatus(user);
    }

    @Override
    public PageInfo findByName(String name, int page, int size) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        //3.调用dao层操作
        PageHelper.startPage(page, size);
        List<User> all = dao.findByName(name);
        for (int i = 0; i < all.size(); i++) {
            if (all.get(i).getStatus() == 0) {
                all.get(i).setStatusStr("关闭");
            } else {
                all.get(i).setStatusStr("开启");
            }
        }
        PageInfo pageInfo = new PageInfo(all);
        return pageInfo;
    }

    @Override
    public User findByEmail(String email) {
        UserDao dao = DaoInstanceFactory.getMapper(UserDao.class);
        User user = dao.findByEmail(email);
        return user;
    }


}
