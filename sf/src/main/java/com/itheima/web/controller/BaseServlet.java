package com.itheima.web.controller;

import com.itheima.domain.system.SysLog;
import com.itheima.service.store.CityService;
import com.itheima.service.store.OrdersService;
import com.itheima.service.store.ProductService;
import com.itheima.service.store.impl.CityServiceImpl;
import com.itheima.service.store.impl.OrdersServiceImpl;
import com.itheima.service.store.impl.ProductServiceImpl;
import com.itheima.service.system.PermissionService;
import com.itheima.service.system.RoleService;
import com.itheima.service.system.SysLogService;
import com.itheima.service.system.UserService;
import com.itheima.service.system.impl.PermissionServiceImpl;
import com.itheima.service.system.impl.RoleServiceImpl;
import com.itheima.service.system.impl.SysLogServiceImpl;
import com.itheima.service.system.impl.UserServiceImpl;
import com.itheima.utils.UUIDUtil;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/BaseServlet")
public class BaseServlet extends HttpServlet {
    protected OrdersService ordersService = null;
    protected ProductService productService = null;
    protected PermissionService permissionService = null;
    protected RoleService roleService = null;
    protected SysLogService sysLogService = null;
    protected UserService userService = null;
    protected CityService cityService = null;

    @Override
    public void init() throws ServletException {
        ordersService = new OrdersServiceImpl();
        productService = new ProductServiceImpl();
        permissionService = new PermissionServiceImpl();
        roleService = new RoleServiceImpl();
        sysLogService = new SysLogServiceImpl();
        userService = new UserServiceImpl();
        cityService = new CityServiceImpl();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String operation = request.getParameter("operation");
            Class clazz = this.getClass();
            Method method = clazz.getMethod(operation, HttpServletRequest.class, HttpServletResponse.class);
            //开始时间
            long startTime = System.currentTimeMillis();

            method.invoke(this, request, response);

            SysLog sysLog = new SysLog();
            //结束时间
            long endTime = System.currentTimeMillis();
            //执行时间
            long time = endTime - startTime;

            sysLog.setExecutionTime(time);
            //获取id
            sysLog.setId(UUIDUtil.getId());
            //访问时间
            /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String format = sdf.format(date);
            sysLog.setVisitTime(format);*/

            //获取用户名
            sysLog.setUsername("username");
            //访问ip
            String remoteAddr = request.getRemoteAddr();
            sysLog.setIp(remoteAddr);
            //访问方法
            String uri = request.getRequestURI();
            String queryString = request.getQueryString();
            int index = queryString.indexOf('&');
            if (index != -1) {
                queryString = queryString.substring(0, index);
            }
            //获取资源url
            sysLog.setUrl(uri);
            uri = uri + "?" + queryString;
            //将获取到的数据截取 = 后 & 前的数据
            String[] split = uri.split("=");
            sysLog.setMethod(split[1]);
            //增删改操作时记录 查询操作时不做记录
            if (!split[1].equals("list")) {
                SysLogService service = new SysLogServiceImpl();
                service.save(sysLog);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}

