package com.itheima.web.controller.store;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.store.City;
import com.itheima.domain.store.Product;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
@WebServlet("/store/city")
public class CityServlet extends BaseServlet {
    public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<City> list = cityService.findAll();
        request.setAttribute("cityList",list);
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(response.getWriter(),list);
    }
}
