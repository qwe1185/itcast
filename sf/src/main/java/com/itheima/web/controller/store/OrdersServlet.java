package com.itheima.web.controller.store;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.store.Orders;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/store/orders")
public class OrdersServlet extends BaseServlet {

    public void list(HttpServletRequest request, HttpServletResponse response) {
        try {
            int page = 1;
            int size = 5;
            if (StringUtils.isNotBlank(request.getParameter("page"))) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            if (StringUtils.isNotBlank(request.getParameter("size"))) {
                size = Integer.parseInt(request.getParameter("size"));
            }
            PageInfo orders = ordersService.selectAll(page, size);
            request.setAttribute("page", orders);
            request.getRequestDispatcher("/pages/store/orders/orders-list.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectAllById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String id = request.getParameter("id");
            Orders orders = ordersService.selectAllById(id);
            request.setAttribute("orders", orders);
            System.out.println(orders);
            request.getRequestDispatcher("/pages/store/orders/orders-show.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String array = request.getParameter("array");
            String[] ids = array.split(",");
            ordersService.deleteOTById(ids);
            ordersService.deleteById(ids);

            response.sendRedirect(request.getContextPath() + "/store/orders?operation=list");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateOpen(HttpServletRequest request, HttpServletResponse response) {
        try {
            String array = request.getParameter("array");
            String[] ids = array.split(",");
            ordersService.updateOpen(ids);
            response.sendRedirect(request.getContextPath() + "/store/orders?operation=list");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateClose(HttpServletRequest request, HttpServletResponse response) {
        try {
            String array = request.getParameter("array");
            String[] ids = array.split(",");
            ordersService.updateClose(ids);
            response.sendRedirect(request.getContextPath() + "/store/orders?operation=list");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void fuzzySearch(HttpServletRequest request, HttpServletResponse response) {
        try {
            int page = 1;
            int size = 5;
            if (StringUtils.isNotBlank(request.getParameter("page"))) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            if (StringUtils.isNotBlank(request.getParameter("size"))) {
                size = Integer.parseInt(request.getParameter("size"));
            }
            String desc = request.getParameter("desc");
            if (StringUtils.isNotBlank(desc)) {
                PageInfo orders = ordersService.fuzzy(page, size, desc);
                request.setAttribute("desc", desc);
                request.setAttribute("page", orders);
                request.getRequestDispatcher("/pages/store/orders/orders-list.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/store/orders?operation=list").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

