package com.itheima.web.controller.store;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.store.Product;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/store/product")
public class ProductServlet extends BaseServlet {
    public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPage = request.getParameter("page");
        String pageSize = request.getParameter("size");
        int current = 1;
        int page = 5;
        if (currentPage != null) {
            current = Integer.parseInt(currentPage);
        };
        if (pageSize != null) {
            page = Integer.parseInt(pageSize);
        };
        PageInfo<Product> byPage = productService.findByPage(current, page);
        request.setAttribute("page", byPage);
        request.getRequestDispatcher("/pages/store/product/product-list.jsp").forward(request,response);
    }
    public void save(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String cityName = request.getParameter("cityName");
        Product product = BeanUtil.fillBean(request, Product.class, "yyyy-MM-dd");
        productService.save(product);
        response.sendRedirect(request.getContextPath()+"/store/product?operation=list");
    }
    public void toEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        Product byId = productService.findById(id);
        request.setAttribute("product",byId);
        request.getRequestDispatcher("/pages/store/product/product-update.jsp").forward(request,response);
    }
    public void edit(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Product product = BeanUtil.fillBean(request, Product.class, "yyyy-MM-dd");
        System.out.println(product);
        productService.update(product);
        response.sendRedirect(request.getContextPath()+"/store/product?operation=list");
    }
    public void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String[] ids = request.getParameterValues("ids");
        productService.delete(ids);
        response.sendRedirect(request.getContextPath()+"/store/product?operation=list");
    }
    public void getMessage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String value = request.getParameter("value");
        Product byId = productService.findById(value);
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(response.getWriter(),byId);
    }
    public void changeStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        String[] split = id.split(",");
        String name = request.getParameter("name");
        productService.changeStatus(split,name);
        response.sendRedirect(request.getContextPath()+"/store/product?operation=list");
    }
    public void findByNum(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String productNum = request.getParameter("productNum");
        List<Product> list = productService.findByLike(productNum);
        System.out.println(list);
        if(list.size() != 0){
            request.setAttribute("list", list);
            request.getRequestDispatcher("/pages/store/product/product-findByNum.jsp").forward(request,response);
        }else {
            request.setAttribute("msg","没有对应商品,请核实后输入");
            request.getRequestDispatcher("/store/product?operation=list").forward(request,response);
        }


    }
}
