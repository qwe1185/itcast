package com.itheima.web.controller.system;

import com.itheima.domain.system.Permission;
import com.itheima.domain.system.User;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.PPMT;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.itheima.web.filters.AutoLoginFilter.getCookieByName;

//pages/home/main.jsp
@WebServlet("/pages/home")
public class LoginServlet extends BaseServlet {
    public static final long serialVersionUID = 1L;

    //登录的方法
    public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //获取用户名和密码
            String username = request.getParameter("username");
            String pwd = request.getParameter("password");
            //创建userService对象
            User user;
            if (StringUtils.isNotBlank(request.getParameter("state"))) {
                User u = (User) request.getSession().getAttribute("user");
                user = userService.aLogin(u.getUsername(), u.getPassword());
            } else {
                user = userService.login(username, pwd);
            }
            //判断user是否为空
            if (user != null) {
                if (1 == (user.getStatus())) {
                    //登陆成功
                    //session经典应用:用户登录成功以后,将用户信息添加到session中
                    request.getSession().setAttribute("user", user);
                    //如果登录成功，加载该用户对应的角色对应的所有模块
                    List<Permission> pmsList = userService.findPermissionById(user.getId());
                    //request.setAttribute("pmsList", pmsList);
                    request.getSession().setAttribute("pmsList", pmsList);

                    //当前登录用户对应的可操作模块的所有url拼接成一个大的字符串
                    StringBuffer sbf = new StringBuffer();
                    for (Permission m : pmsList) {
                        sbf.append(m.getUrl());
                        sbf.append(',');
                    }
                    request.getSession().setAttribute("authorStr", sbf.toString());
                    //1. 判断用户是否勾选了自动登录
                    String ck = request.getParameter("ck");
                    //判断是否勾选
                    if ("1".equals(ck)) {
                        //说明勾选了下次自动登录存入到cookie中
                        Cookie cookie = new Cookie("ckCookie", username + "@" + pwd);
                        //持久化cookie
                        cookie.setMaxAge(7 * 24 * 60 * 60);//保存7天

                        cookie.setPath("/");

                        response.addCookie(cookie);
                    }
                    //重定向到main.jsp
                    response.sendRedirect(request.getContextPath() + "/pages/home/main.jsp");
                } else {
                    if (0 == user.getStatus()) {
                        //登录失败给浏览器响应错误信息
                        request.setAttribute("msg", "该用户未启用,请联系管理员!");

                        //重新转发到登陆页面
                        request.getRequestDispatcher("/login.jsp").forward(request, response);
                    }
                }
            }

            /*if (user == null) {
                //登录失败给浏览器响应错误信息
                request.setAttribute("msg", "用户名或密码错误!");

                //重新转发到登陆页面
                request.getRequestDispatcher("/login.jsp").forward(request, response);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
            //打印日志信息
        }
    }

    //注销的方法
    public void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //移除session中存储的user
        request.getSession().removeAttribute("user");
        //1. 获取指定cookie
        Cookie[] cookies = request.getCookies();
        //获取到cookie
        Cookie ckCookie = getCookieByName(cookies, "ckCookie");
        if (ckCookie != null) {
            Cookie cookie = new Cookie("ckCookie", null);
            cookie.setMaxAge(0);
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        //重定向在登陆页面
        response.sendRedirect(request.getContextPath() + "/login.jsp");
    }

    //修改密码的方法
    public void updatePwd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取id
        String id = request.getParameter("id");
        //String username = request.getParameter("username");
        String pwd = request.getParameter("password");
        String newPwd = request.getParameter("newPassword");
        if (pwd.equals(newPwd)) {
            //调用userService修改方法
            userService.updatePwd(id, pwd);

             /*getServletContext().setAttribute("msg","密码已修改,请重新登录");
            //修改之后/先转发试一下重定向到登陆页面
            request.getRequestDispatcher("/index.jsp").forward(request,response);*/

        } else {
            getServletContext().setAttribute("msg", "两次输入的密码不一致!请重新输入");
            //请求转发到当前页面
            request.getRequestDispatcher("/change-pass.jsp").forward(request, response);
        }

        //重新调用登陆的方法
        //给个修改成功请重新登录的提示
        request.getSession().setAttribute("user", null);

        request.setAttribute("msg", "密码已修改,请重新登录");
        //修改之后/先转发到登陆页面
        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }
}
