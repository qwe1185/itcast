package com.itheima.web.controller.system;


import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Permission;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/system/permission")
public class PermissionServlet extends BaseServlet {

    public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;
        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        PageInfo all = permissionService.findAll(page, size);
        request.setAttribute("page", all);
        request.getRequestDispatcher("/pages/system/permission/permission-list.jsp").forward(request, response);
    }

    public void toAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Permission> all = permissionService.findAll();
        request.setAttribute("permissionlist", all);
        request.getRequestDispatcher("/pages/system/permission/permission-add.jsp").forward(request, response);
    }

    public void save(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Permission permission = BeanUtil.fillBean(request, Permission.class);
        System.out.println(permission);
        permissionService.save(permission);
        //重定向
        response.sendRedirect(request.getContextPath() + "/system/permission?operation=list");
    }

    public void toEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        Permission permission = permissionService.findById(id);
        List<Permission> all = permissionService.findAll();
        request.setAttribute("permission", permission);
        request.setAttribute("permissionList", all);
        request.getRequestDispatcher("/pages/system/permission/permission-update.jsp").forward(request, response);
    }

    public void edit(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Permission permission = BeanUtil.fillBean(request, Permission.class);
        permissionService.update(permission);
        //重定向
        response.sendRedirect(request.getContextPath() + "/system/permission?operation=list");
    }

    public void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Permission permission = BeanUtil.fillBean(request, Permission.class);
        permissionService.delete(permission);
        //重定向
        response.sendRedirect(request.getContextPath() + "/system/permission?operation=list");
    }

    public void deleteById(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String array = request.getParameter("array");
        String[] ids = array.split(",");
        permissionService.deleteOTById(ids);

        response.sendRedirect(request.getContextPath() + "/system/permission?operation=list");

    }

    public void fuzzySearch(HttpServletRequest request, HttpServletResponse response) {
        try {
            int page = 1;
            int size = 5;
            try {
                if (StringUtils.isNotBlank(request.getParameter("page"))) {
                    page = Integer.parseInt(request.getParameter("page"));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            try {
                if (StringUtils.isNotBlank(request.getParameter("size"))) {
                    size = Integer.parseInt(request.getParameter("size"));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            String desc = request.getParameter("desc");
            request.setAttribute("desc", desc);
            if (StringUtils.isNotBlank(desc)) {
                PageInfo orders = permissionService.fuzzy(page, size, desc);
                request.setAttribute("page", orders);
                request.getRequestDispatcher("/pages/system/permission/permission-list.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/system/permission?operation=list").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
