package com.itheima.web.controller.system;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Role;
import com.itheima.service.system.RoleService;
import com.itheima.service.system.impl.RoleServiceImpl;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet("/system/role")
public class RoleServlet extends BaseServlet {
    public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int size = 5;
        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        PageInfo all = roleService.findAll(page, size);


        request.setAttribute("page", all);

        request.getRequestDispatcher("/pages/system/role/role-list.jsp").forward(request, response);

    }

    public void save(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Role role = BeanUtil.fillBean(request, Role.class);

        roleService.save(role);

        response.sendRedirect(request.getContextPath() + "/system/role?operation=list");
    }

    public void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String[] ids = request.getParameterValues("id");
        for (String id : ids) {
            roleService.deleteRolePermission(id);
        }

        roleService.delete(ids);

        //跳转回到页面list
        response.sendRedirect(request.getContextPath() + "/system/role?operation=list");
    }

    public void toUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //查询要修改的数据findById
        String id = request.getParameter("id");
        Role role = roleService.findById(id);
        //将数据加载到指定区域，供页面获取
        request.setAttribute("role", role);
        //跳转页面
        request.getRequestDispatcher("/pages/system/role/role-update.jsp").forward(request, response);
    }


    public void update(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Role role = BeanUtil.fillBean(request, Role.class);

        roleService.update(role);

        //跳转回到页面list
        response.sendRedirect(request.getContextPath() + "/system/role?operation=list");
    }

    public void author(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //获取要授权的角色id
        String roleId = request.getParameter("id");
        //使用id查询对应的数据（角色id对应的模块信息）
        Role role = roleService.findById(roleId);
        request.setAttribute("role", role);
        //根据当前的角色id获取所有的模块数据，并加载关系数据
        List<Map> map = permissionService.findAuthorDataByRoleId(roleId);
        System.out.println(map);
        //map转成json数据
        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(map);
        request.setAttribute("rolePermissionJson", json);
        request.setAttribute("role", role);
        //跳转到树页面中
        request.getRequestDispatcher("/pages/system/role/author.jsp").forward(request, response);
    }

    public void updateRolePermission(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String roleId = request.getParameter("roleId");

        String permissionIds = request.getParameter("permissionIds");
        String[] split = permissionIds.split(",");
        roleService.deleteRolePermission(roleId);
        for (String s : split) {
            roleService.saveRolePermission(roleId, s);
        }
        response.sendRedirect(request.getContextPath() + "/system/role?operation=list");
    }

    public void fuzzy(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int page = 1;
        int size = 5;
        if (StringUtils.isNotBlank(request.getParameter("page"))) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        if (StringUtils.isNotBlank(request.getParameter("size"))) {
            size = Integer.parseInt(request.getParameter("size"));
        }
        String rn = request.getParameter("rn");
        System.out.println(rn);
        if (StringUtils.isNotBlank(rn)) {
            PageInfo all = roleService.fuzzy(page, size, rn);
            request.setAttribute("page", all);
            request.getRequestDispatcher("/pages/system/role/role-list.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/system/role?operation=list");
        }
    }
}
