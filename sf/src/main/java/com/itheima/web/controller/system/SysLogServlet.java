package com.itheima.web.controller.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.SysLog;
import com.itheima.web.controller.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
@WebServlet("/system/syslog")
public class SysLogServlet extends BaseServlet {
    public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //先拿到当前和每页显示条数
        String page = request.getParameter("page");
        String size = request.getParameter("size");
        //设置每页显示条数
        int pageInt = 1;
        int sizeInt = 5;
        System.out.println(page);
        //3.将进行判断
        if (page != null && page != "") {
            pageInt = Integer.parseInt(page);
        }
        if (size != null && size != "") {
            sizeInt = Integer.parseInt(size);
        }
        System.out.println(pageInt);
        PageInfo<SysLog> pageInfo = sysLogService.findPage(pageInt, sizeInt);
        request.setAttribute("page", pageInfo);
        request.getRequestDispatcher("/pages/system/sysLog/syslog-list.jsp").forward(request, response);
    }
    public void downloadReport(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //设置统一编码和返回值类型
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        String fileName = new String("测试文件名.xlsx".getBytes(),"iso8859-1");
        response.addHeader("Content-Disposition","attachment;fileName="+fileName);

        ByteArrayOutputStream os = sysLogService.getReport();
        ServletOutputStream sos = response.getOutputStream();
        os.writeTo(sos);
        sos.flush();
        sos.close();
        os.close();
    }
    public void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("ids");
        String[] ids = id.split(",");
        for (String s : ids) {
            System.out.println(s);
        }
        sysLogService.delete(ids);
        response.sendRedirect(request.getContextPath() + "/system/syslog?operation=list");
    }
}
