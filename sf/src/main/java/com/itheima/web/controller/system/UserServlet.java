package com.itheima.web.controller.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Role;
import com.itheima.domain.system.User;
import com.itheima.utils.BeanUtil;
import com.itheima.web.controller.BaseServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

//uri:/system/user?operation=list
@WebServlet("/system/user")
public class UserServlet extends BaseServlet {
    public void delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] ids = req.getParameterValues("ids");
        userService.deleteSel(ids);
        //重新跳转回list(重定向)
        resp.sendRedirect(req.getContextPath() + "/system/user?operation=list");
    }


    public void toAdd(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*List<Dept> all = deptService.findAll();
        req.setAttribute("deptList",all);*/
        //跳转
        req.getRequestDispatcher("/pages/system/user/user-add.jsp").forward(req, resp);
    }

    public void save(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取数据并封装成user对象(日期转换)
        User user = BeanUtil.fillBean(req, User.class, "yyyy-MM-dd");
        //调用下一层
        userService.save(user);
        //重新跳转回list(重定向)
        resp.sendRedirect(req.getContextPath() + "/system/user?operation=list");
    }

    public void list(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //进入列表页
        //获取数据
        int page = 1;
        int size = 5;
        if (StringUtils.isNotBlank(req.getParameter("page"))) {
            page = Integer.parseInt(req.getParameter("page"));
        }
        if (StringUtils.isNotBlank(req.getParameter("size"))) {
            size = Integer.parseInt(req.getParameter("size"));
        }
        PageInfo all = userService.findAll(page, size);

        //将数据保存到域中
        req.setAttribute("page", all);
        //跳转页面
        req.getRequestDispatcher("/pages/system/user/user-list.jsp").forward(req, resp);
    }

    public void toShow(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*String uid = req.getParameter("uid");
        User user = userService.findById(uid);
        req.setAttribute("user", user);
        req.getRequestDispatcher("/pages/system/user/user-show.jsp").forward(req, resp);*/
        String uid = req.getParameter("uid");
        User user = userService.findById(uid);
        List<Role> roleList = roleService.findAllRoleByUserId(uid);
        user.setRoles(roleList);
        System.out.println(user);
        req.setAttribute("user", user);
        req.getRequestDispatcher("/pages/system/user/user-show.jsp").forward(req, resp);
    }

    public void toRole(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uid = req.getParameter("uid");
        //获取所有的角色列表
        List<Role> roleList = roleService.findAll();
        List<User> userList = userService.findAllRoleByUserId(uid);
        req.setAttribute("uid", uid);
        req.setAttribute("roleList", roleList);
        req.setAttribute("userList", userList);
        req.getRequestDispatcher("/pages/system/user/user-role-add.jsp").forward(req, resp);
    }

    public void addRoleToUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String userId = request.getParameter("userId");
        String[] roleIds = request.getParameterValues("ids");
        userService.updateRole(userId, roleIds);
        //跳转回到页面list
        response.sendRedirect(request.getContextPath() + "/system/user?operation=list");
    }


    public void updateStatus(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        User user = userService.findById(id);
        //获取数据并封装成user对象(日期转换)
        //调用下一层
        userService.updateStatus(user);
        //重新跳转回list(重定向)
        resp.sendRedirect(req.getContextPath() + "/system/user?operation=list");
    }


    public void findByName(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("desc");
        int page = 1;
        int size = 5;
        if (StringUtils.isNotBlank(req.getParameter("page"))) {
            page = Integer.parseInt(req.getParameter("page"));
        }
        if (StringUtils.isNotBlank(req.getParameter("size"))) {
            size = Integer.parseInt(req.getParameter("size"));
        }
        PageInfo all = userService.findByName(name, page, size);
        //将数据保存到域中
        req.setAttribute("page", all);
        req.setAttribute("desc", name);
        //跳转页面
        req.getRequestDispatcher("/pages/system/user/user-list.jsp").forward(req, resp);
    }

    public void emailCheck(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");

        System.out.println("1:" + email);

        User u = userService.findByEmail(email);

        System.out.println(u);

        //4. 判断User是否存在, 返回不同的提示信息
        PrintWriter writer = resp.getWriter();
        if (u == null) {
            // 不存在, 可以是可用
            writer.write("true");
        } else {
            // 存在, 可以是不可用
            writer.write("false");
        }
    }
}


