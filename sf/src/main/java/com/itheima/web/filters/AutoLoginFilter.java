package com.itheima.web.filters;

import com.itheima.domain.system.User;
import com.itheima.service.system.UserService;
import com.itheima.service.system.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class AutoLoginFilter implements Filter {

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        try {
            //先进型类型转换
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) resp;
            //System.out.println("拦截器拦截了");
            //判断用户是否已经登录了
            Object us = request.getSession().getAttribute("user");
            //System.out.println(us);
            if (us!=null){
                //说明已经登陆过了,自动登录
                chain.doFilter(request, response);
                return;
            }
            //1. 获取指定cookie
            Cookie[] cookies = request.getCookies();
            //获取到cookie
            Cookie ckCookie = getCookieByName(cookies, "ckCookie");
            //判断ckCookie是否存在
            if (ckCookie == null){
                //说明没有直接放行
                chain.doFilter(request,response);
                return;
            }
            //如果有就获取到cookie中的用户名和密码
            String value = ckCookie.getValue();
            String username = value.split("@")[0];
            String pwd = value.split("@")[1];
            //在调用service查询用户,判断用户是否存在(用户有可能修改了密码)
            UserService service = new UserServiceImpl();

            User user = service.login(username, pwd);
            //看看是否能查到
            //System.out.println(user);
            if (user == null){
                //说明没有直接放行
                chain.doFilter(request,response);

                return;
            }
            // 若有: 将用户信息添加到session域中
            request.getSession().setAttribute("user", user);
            //最后在放行
            chain.doFilter(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //根据名字获取cookie的方法
    public  static  Cookie getCookieByName(Cookie[] cookies,String cookieName){
        //如果cookies不为null说明有值就遍历cookies
        if (cookies!=null) {
            for (Cookie cookie : cookies) {
                if(cookieName.equals(cookie.getName())){
                    //找到指定cookie
                    return cookie;
                }
            }
        }
        return null;
    }



    public void init(FilterConfig config) throws ServletException {

    }
    public void destroy() {
    }

}
