<%@ page import="org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHdrFtrRef" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="pages/base.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>数据 - AdminLTE2定制版 | Log in</title>

    <meta
            content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"
            name="viewport">

    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/ionicons/css/ionicons.min.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/adminLTE/css/AdminLTE.css">
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/plugins/iCheck/square/blue.css">
    <script src="validate/jquery-1.11.0.min.js"></script>
    <script src="validate/jquery.validator.min.js"></script>
    <script src="validate/local/zh-CN.js"></script>
    <link href="validate/jquery.validator.css" rel="stylesheet">
</head>

<body class="hold-transition login-page">


<div class="login-box">
    <div class="login-logo">
        <a href="all-admin-index.html"><b>修改密码</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"></p>
        <c:if test="${not empty msg}">
            <!-- 出错显示的信息框 -->
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span>&times;</span></button>
                <strong>${msg}</strong>
            </div>
        </c:if>

        <form action="${pageContext.request.contextPath}/pages/home?operation=updatePwd" method="post"
              id="formId" action="results.php" autocomplete="off"
              data-validator-option="{theme:'yellow_right_effect',stopOnError:true}">
            <input type="hidden" id="id" name="id" value="${user.id}"/>
            <div class="form-group has-feedback">
                <input type="text" data-rule="用户名:required;username" name="username" class="form-control" value="${user.username}"
                       placeholder="用户名" disabled="disabled"> <span
                    class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" value="${password}"
                       placeholder="新密码"> <span
                    class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" data-rule="密码:required;password" name="newPassword" class="form-control" value="${pwd}"
                       placeholder="确认新密码"> <span
                    class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <!-- /.col -->

                <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">修改</button>
                    <%--<a class="btn btn-primary btn-block btn-flat" href="JavaScript:update()">修改</a>--%>

                </div>

                <div class="col-xs-4" style="margin-left: 116px">
                    <button type="submit" style="float: right" class="btn btn-primary btn-block btn-flat" onclick="history.go(-1)">返回</button>
                </div>
                <!-- /.col -->
            </div>

        </form>

    </div>

</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<!-- Bootstrap 3.3.6 -->
<!-- iCheck -->

<%--<script>
    function update() {
        if(confirm("您确定修改密码么?")){
            location.href ="${pageContext.request.contextPath}/pages/home?operation=updatePwd"
        }
    }


</script>--%>
<script
        src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script
        src="${pageContext.request.contextPath}/plugins/bootstrap/js/bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/plugins/iCheck/icheck.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>

</html>