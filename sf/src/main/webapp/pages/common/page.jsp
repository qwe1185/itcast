<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<body>
<div class="pull-left">
    <div class="form-group form-inline">
        <%--总共${page.pages} 页，共${page.total} 条数据。--%>
        总共${page.pages} 页，共${page.total} 条数据。 每页
        <select class="form-control" onchange="goSize(this)">
            <option value="2" ${page.pageSize eq "2" ? "selected" : ""}>2</option>
            <option value="4" ${page.pageSize eq "4" ? "selected" : ""}>4</option>
            <option value="5" ${page.pageSize eq "5" ? "selected" : ""}>5</option>
            <option value="6" ${page.pageSize eq "6" ? "selected" : ""}>6</option>
            <option value="10" ${page.pageSize eq "10" ? "selected" : ""}>10</option>
            <option value="20" ${page.pageSize eq "20" ? "selected" : ""}>20</option>
        </select> 条
    </div>
</div>

<div class="box-tools pull-right">
    <ul class="pagination">
        <li>
            <a href="javascript:goPage(1)" aria-label="Previous">首页</a>
        </li>
        <li>
            <a href="javascript:goPage(${page.prePage})">上一页</a>
        </li>
        <c:forEach begin="${page.navigateFirstPage}" end="${page.navigateLastPage}" var="i">
            <li class="paginate_button ${page.pageNum==i ? 'active':''}">
                <a href="javascript:goPage(${i})">${i}</a>
            </li>
        </c:forEach>
        <li>
            <a href="javascript:goPage(${page.nextPage})">下一页</a>
        </li>
        <li>
            <a href="javascript:goPage(${page.pages})" aria-label="Next">尾页</a>
        </li>
    </ul>
</div>
<form id="pageForm" action="${param.pageUrl}" method="post">
    <input type="hidden" name="page" id="pageNum">
    <input type="hidden" name="size" value="${page.pageSize}">
    <input type="hidden" value="${desc}" name="desc">
</form>
<form id="sizeForm" action="${param.pageUrl}" method="post">
    <input type="hidden" name="size" id="size">
    <input type="hidden" value="${desc}" name="desc">
</form>

<script>
    function goPage(page) {
         document.getElementById("pageNum").value = page;
        document.getElementById("pageForm").submit();
    }

    function goSize(size) {
        document.getElementById("size").value = size.value;
        document.getElementById("sizeForm").submit();
    }
</script>
</body>
</html>
