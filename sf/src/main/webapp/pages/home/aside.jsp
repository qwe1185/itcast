<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>

<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="${pageContext.request.contextPath}/img/sf.jpg"
                     class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>${user.username}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> 在线</a>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="header">菜单</li>
            <li id="admin-index">
                <a href="${pageContext.request.contextPath}/pages/home/main.jsp">
                    <i class="fa fa-dashboard"></i>
                    <span>首页</span></a>
            </li>
            <c:forEach items="${pmsList}" var="item">
            <c:if test="${item.ctype==0}">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cube"></i> <span>${item.permissionName}</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                </a>
                <ul class="treeview-menu">
                    <c:forEach items="${pmsList}" var="item2">
                        <c:if test="${item2.ctype==1 && item2.parentId == item.id}">
                            <li id="${item2.id}">
                                <a onclick="setSidebarActive(this)"
                                   href="${pageContext.request.contextPath}${item2.url}?operation=list" target="iframe">
                                    <i class="fa fa-circle-o"></i>${item2.permissionName}
                                </a>
                            </li>
                        </c:if>
                    </c:forEach>
                </ul>
            </li>
            </c:if>
            </c:forEach>
    </section>
</aside>