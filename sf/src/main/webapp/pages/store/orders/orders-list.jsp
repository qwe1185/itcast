<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="../../base.jsp" %>
<!DOCTYPE html>
<html>

<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>
<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            数据管理
            <small>数据列表</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li><a href="#">数据管理</a></li>
            <li class="active">数据列表</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content">

        <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">列表</h3>
            </div>
            <div class="box-body">
                <!-- 数据表格 -->
                <div class="table-box">
                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <%--<button type="button" class="btn btn-default" title="新建"--%>
                                <%--onclick="location.href='/pages/store/product/product-add.jsp'">--%>
                                <%--<i class="fa fa-file-o"></i> 新建--%>
                                <%--</button>--%>
                                <button type="button" class="btn btn-default" title="删除" onclick="deleteById()">
                                    <i class="fa fa-trash-o"></i> 删除
                                </button>
                                <button type="button" class="btn btn-default" title="开启" onclick="changeOpen()">
                                    <i class="fa fa-check"></i> 开启
                                </button>
                                <button type="button" class="btn btn-default" title="屏蔽" onclick="changeClose()">
                                    <i class="fa fa-ban"></i> 屏蔽
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick="window.location.reload()">
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <%--                                <input type="text" class="form-control input-sm"--%>
                            <%--                                       placeholder="搜索"> <span--%>
                            <%--                                    class="glyphicon glyphicon-search form-control-feedback"></span>--%>
                            <form action="${pageContext.request.contextPath}/store/orders?operation=fuzzySearch"
                                  method="post">
                                <input type="text" placeholder="搜索" name="desc" value="${desc}">
                                <button type="submit" value="搜索">搜索</button>
                            </form>
                        </div>
                    </div>
                    <!--工具栏/-->

                    <!--数据列表-->
                    <table id="dataList"
                           class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th class="" style="padding-right: 0px;"><input
                                    id="selall" type="checkbox" class="icheckbox_square-blue">
                            </th>
                            <th class="sorting_asc">序号</th>
                            <th class="sorting_desc">订单编号</th>
                            <th class="sorting_asc sorting_asc_disabled">产品名称</th>
                            <th class="sorting_desc sorting_desc_disabled">金额</th>
                            <th class="sorting">下单时间</th>
                            <th class="text-center sorting">订单状态</th>
                            <th class="text-center">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${page.list}" var="order" varStatus="count">
                            <tr>
                                <td><input name="ids" type="checkbox" value="${order.id}"></td>
                                <td>${count.count}</td>
                                <td>${order.orderNum}</td>
                                <td>${order.product.productName}</td>
                                <td>${order.product.productPrice}</td>
                                <td>${order.orderTimeStr}</td>
                                <td class="text-center">${order.orderStatus==1?"开启":"屏蔽"}</td>
                                <td class="text-center">
                                    <button type="button" class="btn bg-olive btn-xs"
                                            onclick="location.href='${pageContext.request.contextPath}/store/orders?operation=selectAllById&id=${order.id}'">
                                        详情
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <!--数据列表/-->
                </div>
                <!-- 数据表格 /-->
            </div>
            <!-- /.box-body -->
            <!-- .box-footer-->
            <div class="box-footer">
                <jsp:include page="../../common/page.jsp">
                    <jsp:param
                            value="${empty desc ? '/store/orders?operation=list' : '/store/orders?operation=fuzzySearch'}"
                            name="pageUrl"/>
                </jsp:include>
            </div>
            <!-- /.box-footer-->
        </div>
    </section>
    <!-- 正文区域 /-->
</div>
<script>
    function deleteById() {
        var checks = $("input:checkbox:checked");
        if(checks.length==0){
            alert("请勾选至少一条数据!!!");
            return;
        }
        if (window.confirm("确认删除选中?")) {
            if (checks.length != 0) {
                var array = new Array();
                checks.each(function (index) {
                    var i = checks[index].value;
                    array.push(i);
                });
                location.href = "${pageContext.request.contextPath}/store/orders?operation=deleteById&array=" + array
            }
        }
    }

    function changeOpen() {
        var checks = $("input:checkbox:checked");
        if(checks.length==0){
            alert("请勾选至少一条数据!!!");
            return;
        }
        if (window.confirm("确认开启状态?")) {
            if (checks.length != 0) {
                var array = new Array();
                checks.each(function (index) {
                    var i = checks[index].value;
                    array.push(i);
                });
                location.href = "${pageContext.request.contextPath}/store/orders?operation=updateOpen&array=" + array
            }
        }
    }

    function changeClose() {
        var checks = $("input:checkbox:checked");
        if(checks.length==0){
            alert("请勾选至少一条数据!!!");
            return;
        }
        if (window.confirm("确认屏蔽状态?")) {
            if (checks.length != 0) {
                var array = new Array();
                checks.each(function (index) {
                    var i = checks[index].value;
                    array.push(i);
                });
                location.href = "${pageContext.request.contextPath}/store/orders?operation=updateClose&array=" + array
            }
        }
    }
</script>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    function changePageSize() {
        //获取下拉框的值
        var pageSize = $("#changePageSize").val();

        //向服务器发送请求，改变没页显示条数
        location.href = "${pageContext.request.contextPath}/store/orders?operation=list&page=1&size="
            + pageSize;
    }

    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {

        // 激活导航位置
        setSidebarActive("admin-datalist");

        // 列表按钮
        $("#dataList td input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });
    });
</script>
</body>

</html>