<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="../../base.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
    <script src="/validate/jquery-1.11.0.min.js"></script>
    <script src="/validate/jquery.validator.min.js"></script>
    <script src="/validate/local/zh-CN.js"></script>
    <link href="/validate/jquery.validator.css" rel="stylesheet">
</head>

<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            产品管理
            <small>产品表单</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                    class="fa fa-dashboard"></i> 首页</a></li>
            <li><a
                    href="${pageContext.request.contextPath}/product/findAll.do">产品管理</a></li>
            <li class="active">产品表单</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <form action="${pageContext.request.contextPath}/store/product?operation=save"
          method="post">
        <!-- 正文区域 -->
        <section class="content"> <!--产品信息-->

            <div class="panel panel-default">
                <div class="panel-heading">产品信息</div>
                <div class="row data-type">

                    <div class="col-md-2 title">产品编号</div>
                    <div class="col-md-4 data">
                        <input type="text" data-rule="产品编号:required;" class="form-control" name="productNum"
                               placeholder="产品编号" value="">
                    </div>
                    <div class="col-md-2 title">产品名称</div>
                    <div class="col-md-4 data">
                        <input type="text" data-rule="产品名称:required;" class="form-control" name="productName"
                               placeholder="产品名称" value="">
                    </div>
                    <div class="col-md-2 title">出发时间</div>
                    <div class="col-md-4 data">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="date" data-rule="出发时间:required;" class="form-control pull-right"
                                   id="datepicker-a3" name="departureTime">
                        </div>
                    </div>


                    <%--<div class="col-md-2 title">出发城市</div>
                    <div id="chooseCity" class="col-md-4 data">
                        <select class="form-control select2" style="width: 100%" id="cityName" name="cityName">
                            <option>请选择</option>
                            <c:forEach items="${cityList}" var="city">
                                <option  value="${city.cname}">${city.cname}</option>
                            </c:forEach>
                        </select>
                    </div>--%>
                    <div class="col-md-2 title">出发城市</div>
                    <div class="col-md-4 data">
                        <select class="form-control select2" style="width: 100%" id="chooseCity" name="cityName">
                            <option>请选择</option>
                        </select>
                    </div>

                    <div class="col-md-2 title">产品价格</div>
                    <div class="col-md-4 data">
                        <input type="text" data-rule="产品价格:required;" class="form-control" placeholder="产品价格"
                               name="productPrice">
                    </div>

                    <div class="col-md-2 title">产品状态</div>
                    <div class="col-md-4 data">
                        <select class="form-control select2" style="width: 100%"
                                name="productStatus">
                            <option value="0" selected="selected">关闭</option>
                            <option value="1">开启</option>
                        </select>
                    </div>

                    <div class="col-md-2 title rowHeight2x">其他信息</div>
                    <div class="col-md-10 data rowHeight2x">
							<textarea class="form-control" rows="3" placeholder="其他信息"
                                      name="productDesc"></textarea>
                    </div>

                </div>
            </div>
            <!--订单信息/--> <!--工具栏-->
            <div class="box-tools text-center">
                <button type="submit" class="btn bg-maroon">保存</button>
                <button type="button" class="btn bg-default"
                        onclick="history.back(-1);">返回
                </button>
            </div>
            <!--工具栏/--> </section>
        <!-- 正文区域 /-->
    </form>
</div>
<!-- 内容区域 /-->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    $(document).ready(function () {

        var elementById = document.getElementById("chooseCity");
        $.get(
            "/store/city?operation=list",
            function (data) {
                for (var i = 0; i < data.length; i++) {
                    var option = document.createElement("option");
                    option.innerText = data[i].cname;
                    option.value = data[i].cname;
                    elementById.appendChild(option);
                }
            },
            "json"
        )
    });
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {
        $('#datepicker-a3').datetimepicker({
            format: "yyyy-mm-dd hh:ii",
            autoclose: true,
            todayBtn: true,
            language: "zh-CN"
        });
    });

    $(document).ready(function () {
        // 激活导航位置
        setSidebarActive("order-manage");
        $("#datepicker-a3").datetimepicker({
            format: "yyyy-mm-dd hh:ii",

        });

    });

    $(document).ready(function () {
        $.ajax({
            type: 'post',
            url: '${pageContext.request.contextPath}/city/findAll',
            success: function (data) {
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].cname;
                    $("#cityName").append("<option value='" + name + "'>" + name + "</option>");
                }
            }
        })
    });

</script>


</body>

</html>