<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="../../base.jsp" %>
<script src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!DOCTYPE html>
<html>

<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>

<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">

    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            数据管理
            <small>数据列表</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> 首页</a></li>
            <li><a href="#">数据管理</a></li>
            <li class="active">数据列表</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content">

        <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">列表</h3>
            </div>

            <div class="box-body">

                <!-- 数据表格 -->
                <div class="table-box">

                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="新建"
                                        onclick="location.href='${pageContext.request.contextPath}/pages/store/product/product-add.jsp'">
                                    <i class="fa fa-file-o"></i> 新建
                                </button>
                                <button type="button" class="btn btn-default" title="删除"
                                        onclick="deleteBySelect()">
                                    <i class="fa fa-trash-o"></i> 删除
                                </button>
                                <button type="button" class="btn btn-default" title="开启"
                                        onclick="openBySelect()">
                                    <i class="fa fa-check"></i> 开启
                                </button>
                                <button type="button" class="btn btn-default" title="屏蔽"
                                        onclick="shieldBySelect()">
                                    <i class="fa fa-ban"></i> 屏蔽
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick="location.href='${pageContext.request.contextPath}/store/product?operation=list'">
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                                <button type="button" class="btn btn-default" title="返回"
                                        onclick="history.go(-1)">
                                    <i class="fa fa-refresh"></i> 返回
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                    </div>
                    <!--工具栏/-->

                    <!--数据列表-->
                    <form action="${pageContext.request.contextPath}/store/product?operation=delete" id="formSub"
                          method="post">
                        <table id="dataList"
                               class="table table-bordered table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="" style="padding-right: 0px;">
                                    <input id="selall" type="checkbox" class="icheckbox_square-blue">
                                </th>
                                <th class="sorting_asc">ID</th>
                                <th class="sorting_desc">编号</th>
                                <th class="sorting_asc sorting_asc_disabled">产品名称</th>
                                <th class="sorting_desc sorting_desc_disabled">出发城市</th>
                                <th class="sorting">出发时间</th>
                                <th class="text-center sorting">产品价格</th>
                                <th class="sorting">产品描述</th>
                                <th class="text-center sorting">状态</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${list}" var="product" varStatus="v">
                                <tr>
                                    <td><input id="did" name="ids" type="checkbox" value="${product.id}"></td>
                                    <td>${product.id}</td>
                                    <td>${product.productNum}</td>
                                    <td>${product.productName}</td>
                                    <td>${product.cityName}</td>
                                    <td>${product.departureTime}</td>
                                    <td class="text-center">${product.productPrice}</td>
                                    <td>${product.productDesc}</td>
                                    <c:if test="${product.productStatus == 0}">
                                        <td class="text-center">关闭</td>
                                    </c:if>
                                    <c:if test="${product.productStatus == 1}">
                                        <td class="text-center">开启</td>
                                    </c:if>
                                    <td class="text-center">
                                        <button type="button" class="btn bg-olive btn-xs"
                                                onclick="location.href='${pageContext.request.contextPath}/store/product?operation=toEdit&id=${product.id}'">
                                            编辑
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <!--数据列表/-->
                    </form>

                </div>
                <!-- 数据表格 /-->
            </div>

        </div>
    </section>
    <!-- 正文区域 /-->
</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    function shieldBySelect() {
        var che = $("[name = ids]:checked");
        var check = "";
        for (var i = 0; i < che.length; i++) {
            if (che[i].checked) {
                check = check + che[i].value + ",";
            }
        }
        if (check.length == 0) {
            confirm("请添加后再进行操作")
        } else {
            var b = confirm("您确认要屏蔽以下产品?");
            if (b) {
                location.href = "/store/product?operation=changeStatus&name=shield&id=" + check;
            } else {
                che.each(function () {
                    $(this).prop("checked", false)
                })
            }
        }
    }

    function openBySelect() {
        var che = $("[name = ids]:checked");
        var check = "";
        for (var i = 0; i < che.length; i++) {
            if (che[i].checked) {
                check = check + che[i].value + ",";
            }
        }
        if (check.length == 0) {
            confirm("请添加后再进行操作")
        } else {
            var b = confirm("您确认要开启以下产品?");
            if (b) {
                location.href = "/store/product?operation=changeStatus&name=open&id=" + check;
            } else {
                che.each(function () {
                    $(this).prop("checked", false)
                })
            }
        }
    }

    function deleteBySelect() {
        var del = $("[name = ids]:checked");
        if (del.length == 0) {
            confirm("请添加后删除")
        } else {
            var b = confirm("您确认要删除吗?");
            if (b) {
                $("#formSub").submit();
            } else {
                del.each(function () {
                    $(this).prop("checked", false)
                })
            }
        }
    }


    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {

        // 激活导航位置
        setSidebarActive("admin-datalist");

        // 列表按钮
        $("#dataList td input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });
    });
</script>
</body>

</html>