<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="../../base.jsp" %>
<script src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>

<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">

    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            产品管理
            <small>产品表单</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                    class="fa fa-dashboard"></i> 首页</a></li>
            <li><a
                    href="${pageContext.request.contextPath}/product/findAll.do">产品管理</a></li>
            <li class="active">产品表单</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <form action="${pageContext.request.contextPath}/store/product?operation=edit"
          method="post">
        <!-- 正文区域 -->
        <section class="content"> <!--产品信息-->

            <div class="panel panel-default">
                <div class="panel-heading">产品信息</div>
                <div class="row data-type">
                    <input type="hidden" name="id" id="idHidden" value="${product.id}">
                    <div class="col-md-2 title">产品编号</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="productNum"
                               placeholder="产品编号" value="${product.productNum}" readonly="readonly">
                    </div>
                    <div class="col-md-2 title">产品名称</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="productName"
                               placeholder="产品名称" value="${product.productName}">
                    </div>
                    <div class="col-md-2 title">出发时间</div>
                    <div class="col-md-4 data">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="date" class="form-control pull-right"
                                   id="datepicker-a3" name="departureTime" value=${product.departureTimeStr}>
                        </div>
                    </div>
                    <div class="col-md-2 title">出发城市</div>
                    <div class="col-md-4 data">
                        <select class="form-control select2" style="width: 100%" id="chooseCity" name="cityName">
                            <option>请选择</option>
                        </select>
                    </div>

                    <div class="col-md-2 title">产品价格</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" placeholder="产品价格"
                               name="productPrice" value="${product.productPrice}">
                    </div>

                    <div class="col-md-2 title">产品状态</div>
                    <div class="col-md-4 data">
                        <select class="form-control select2" style="width: 100%"
                                name="productStatus">
                            <option value="0" ${product.productStatus == 0 ? "selected" : ""}>关闭</option>
                            <option value="1" ${product.productStatus == 1 ? "selected" : ""}>开启</option>
                        </select>
                    </div>

                    <div class="col-md-2 title rowHeight2x">其他信息</div>
                    <div class="col-md-10 data rowHeight2x">
							<textarea class="form-control" rows="3" placeholder="其他信息"
                                      name="productDesc">${product.productDesc}</textarea>
                    </div>
                </div>
            </div>
            <!--订单信息/--> <!--工具栏-->
            <div class="box-tools text-center">
                <button type="submit" class="btn bg-maroon">修改</button>
                <button type="button" class="btn bg-default"
                        onclick="history.back(-1);">返回
                </button>
            </div>
            <!--工具栏/--> </section>
        <!-- 正文区域 /-->
    </form>
</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    $(document).ready(function () {
        var elementById = document.getElementById("chooseCity");
        $.get(
            "/store/city?operation=list",
            function (data) {
                for (var i = 0; i < data.length; i++) {
                    var option = document.createElement("option");
                    option.innerText = data[i].cname;
                    option.value = data[i].cname;
                    option.className = "cN"
                    elementById.appendChild(option);
                }
            },
            "json"
        );
        var byId = document.getElementById("idHidden");
        var value = byId.value;
        var elementsByClassName = document.getElementsByClassName("cN");

        $.get(
            "/store/product?operation=getMessage",
            {value: value},
            function (data) {
                var s2 = JSON.stringify(data.cityName);
                for (var i = 0; i < elementsByClassName.length; i++) {
                    var s = JSON.stringify(elementsByClassName[i].value);
                    if (s2 == s) {
                        elementsByClassName[i].setAttribute("selected", true)
                        return;
                    }
                }
            },
            "json"
        )
    });


    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {
        $('#datepicker-a3').datetimepicker({
            format: "yyyy-mm-dd hh:ii",
            autoclose: true,
            todayBtn: true,
            language: "zh-CN"
        });
    });

    $(document).ready(function () {
        // 激活导航位置
        setSidebarActive("order-manage");
        $("#datepicker-a3").datetimepicker({
            format: "yyyy-mm-dd hh:ii",

        });

    });


</script>


</body>

</html>