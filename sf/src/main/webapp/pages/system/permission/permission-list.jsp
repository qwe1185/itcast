<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="../../base.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>
<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            资源权限管理
            <small>全部权限</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="../index.jsp"><i
                    class="fa fa-dashboard"></i> 首页</a></li>
            <li><a
                    href="../permission/findAll.do">资源权限管理</a></li>
            <li class="active">全部权限</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content"> <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">列表</h3>
            </div>

            <div class="box-body">

                <!-- 数据表格 -->
                <div class="table-box">

                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="新建"
                                        onclick='location.href="${pageContext.request.contextPath}/system/permission?operation=toAdd"'>
                                    <i class="fa fa-file-o"></i> 新建
                                </button>
                                <button type="button" class="btn btn-default" title="删除" onclick="deleteById()">
                                    <i class="fa fa-trash-o"></i> 删除
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick='location.href="${pageContext.request.contextPath}/system/permission?operation=list"'>
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <form action="${pageContext.request.contextPath}/system/permission?operation=fuzzySearch"
                              method="post">
                            <input type="text" placeholder="搜索" name="desc" value="${desc}">
                            <button type="submit" value="搜索">搜索</button>
                        </form>
                    </div>
                    <!--工具栏/-->

                    <!--数据列表-->
                    <table id="dataList"
                           class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th class="" style="padding-right: 0px">
                                <input id="selall" type="checkbox" class="icheckbox_square-blue">
                            </th>
                            <th class="sorting_asc">模块名</th>
                            <th class="sorting">类型</th>
                            <th class="sorting_asc">上级模块</th>
                            <th class="sorting_asc sorting_asc_disabled">url</th>
                            <th class="text-center">操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${page.list}" var="o" varStatus="st">
                            <tr>
                                <td><input name="ids" type="checkbox" name="id" value="${o.id}"></td>
                                <td><a href="#">${o.permissionName}</a></td>
                                <td>${o.ctype==0?'主菜单':o.ctype==1?'二级菜单':'按钮'}</td>
                                <td>${o.permission.permissionName}</td>
                                <td>${o.url}</td>
                                <td class="text-center">
                                    <a href="${pageContext.request.contextPath}/system/permission?operation=toEdit&id=${o.id}"
                                       class="btn bg-olive btn-xs">编辑</a>
                                    <a href="${pageContext.request.contextPath}/system/permission?operation=delete&id=${o.id}"
                                       class="btn bg-olive btn-xs">删除权限</a>
                                </td>
                            </tr>
                        </c:forEach>
                        <%--<c:forEach items="${permissionList}" var="p">
                            <tr>
                                <td><input name="ids" type="checkbox"></td>
                                <td>${p.id }</td>
                                <td>${p.permissionName }</td>
                                <td>${p.url }</td>
                                <td class="text-center">
                                    <a href="../permission/findById.do?id=${p.id}" class="btn bg-olive btn-xs">详情</a>
                                    <a href="../permission/deletePermission.do?id=${p.id}" class="btn bg-olive btn-xs">删除权限</a>
                                </td>
                            </tr>
                        </c:forEach>--%>
                        </tbody>
                    </table>
                    <!--数据列表/-->
                </div>
                <!-- 数据表格 /-->
            </div>
            <!-- /.box-body -->

            <!-- .box-footer-->
            <div class="box-footer">
                <jsp:include page="../../common/page.jsp">
                    <jsp:param
                            value="${pageContext.request.contextPath}/${empty desc ? 'system/permission?operation=list' : 'system/permission?operation=fuzzySearch'}"
                            name="pageUrl"/>
                </jsp:include>
            </div>
            <!-- /.box-footer-->

        </div>

    </section>
    <!-- 正文区域 /-->

</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document)
        .ready(
            function () {

                // 激活导航位置
                setSidebarActive("admin-datalist");

                // 列表按钮
                $("#dataList td input[type='checkbox']")
                    .iCheck(
                        {
                            checkboxClass: 'icheckbox_square-blue',
                            increaseArea: '20%'
                        });
                // 全选操作
                $("#selall")
                    .click(
                        function () {
                            var clicks = $(this).is(
                                ':checked');
                            if (!clicks) {
                                $(
                                    "#dataList td input[type='checkbox']")
                                    .iCheck(
                                        "uncheck");
                            } else {
                                $(
                                    "#dataList td input[type='checkbox']")
                                    .iCheck("check");
                            }
                            $(this).data("clicks",
                                !clicks);
                        });
            });

    function deleteById() {
        var checks = $("input:checkbox:checked");
        if (checks.length != 0) {
            if (window.confirm("确认删除选中?")) {
                var array = new Array();
                checks.each(function (index) {
                    var i = checks[index].value;
                    array.push(i);
                });
                location.href = "${pageContext.request.contextPath}/system/permission?operation=deleteById&array=" + array
            }
        } else {
            alert("请勾选至少一条数据!!!")
        }
    }

</script>
</body>

</html>