<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="../../base.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>
<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">

    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            资源权限管理
            <small>资源权限表单</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                    class="fa fa-dashboard"></i> 首页</a></li>
            <li><a href="${pageContext.request.contextPath}/permission/findAll.do">资源权限管理</a></li>
            <li class="active">资源权限表单</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <form action="${ctx}/system/permission?operation=edit" method="post">
        <input type="hidden" name="id" value="${permission.id}">
        <!-- 正文区域 -->
        <section class="content"> <!--产品信息-->

            <div class="panel panel-default">
                <div class="panel-heading">资源权限信息</div>
                <div class="row data-type">

                    <div class="col-md-2 title">权限名称</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="permissionName"
                               placeholder="权限名称" value="${permission.permissionName}">
                    </div>


                    <div class="col-md-2 title">RUL</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="url"
                               placeholder="URL" value="${permission.url}">
                    </div>

                    <div class="col-md-2 title">上级模块</div>
                    <div class="col-md-4 data">
                        <select class="form-control" name="parentId">
                            <option value="">请选择</option>
                            <c:forEach items="${permissionList}" var="item">
                                <option ${permission.parentId == item.id ?'selected':''} value="${item.id}">
                                        ${item.permissionName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="col-md-2 title">类型</div>
                    <div class="col-md-4 data">
                        <div class="form-group form-inline">
                            <div class="radio"><label><input type="radio" ${permission.ctype==0?'checked':''}
                                                             name="ctype" value="0">主菜单</label></div>
                            <div class="radio"><label><input type="radio" ${permission.ctype==1?'checked':''}
                                                             name="ctype" value="1">二级菜单</label></div>
                            <div class="radio"><label><input type="radio" ${permission.ctype==2?'checked':''}
                                                             name="ctype" value="2">按钮</label></div>
                        </div>
                    </div>


                </div>
            </div>
            <!--订单信息/--> <!--工具栏-->
            <div class="box-tools text-center">
                <button type="submit" class="btn bg-maroon">保存</button>
                <button type="button" class="btn bg-default"
                        onclick="history.back(-1);">返回
                </button>
            </div>
            <!--工具栏/--> </section>
        <!-- 正文区域 /-->
    </form>
</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

</script>


</body>

</html>