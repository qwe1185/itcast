<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="../../base.jsp" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">


    <script>


        //删除选中
        function deleteSel() {
            //1.获取选中的复选框的个数
            var length = $("[name='id']:checked").length;
            //判断
            if (length < 1) {
                alert("请选择您要删除的数据!");
                return;
            }
            //询问是否删除
            if (confirm("您确认要删除么?")) {
                //提交
                $("#formId").submit();
            }
        }
    </script>


</head>

<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            角色管理
            <small>全部角色</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="/index.jsp">
                    <i class="fa fa-dashboard"></i> 首页
                </a>
            </li>
            <li>
                <a href="/role/findAll.do">角色管理</a>
            </li>
            <li class="active">全部角色</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content"> <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">列表</h3>
            </div>
            <div class="box-body">
                <!-- 数据表格 -->
                <div class="table-box">
                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="新建"
                                        onclick="location.href='${pageContext.request.contextPath}/pages/system/role/role-add.jsp'">
                                    <i class="fa fa-file-o"></i> 新建
                                </button>
                                <button type="button" class="btn btn-default" title="删除"
                                        onclick="deleteSel()">
                                    <i class="fa fa-trash"></i> 删除
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick='location.href="${pageContext.request.contextPath}/system/role?operation=list"'>
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <form action="${pageContext.request.contextPath}/system/role?operation=fuzzy" method="post">
                                <input type="text" placeholder="搜索" name="rn">
                                <button type="submit" value="搜索">搜索</button>
                            </form>
                        </div>
                    </div>
                    <!--工具栏/-->

                    <!--数据列表-->
                    <form action="${pageContext.request.contextPath}/system/role?operation=delete" method="post"
                          id="formId">
                        <table id="dataList" class="table table-bordered table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="" style="padding-right: 0px"><input
                                        id="selall" type="checkbox" class="icheckbox_square-blue">
                                </th>
                                <th class="sorting_asc">序号</th>
                                <th class="sorting_desc">角色名称</th>
                                <th class="sorting_asc sorting_asc_disabled">描述</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>

                            <%--        <tr>
                                        <td><input name="ids" type="checkbox"></td>

                                        <td>1</td>
                                        <td>班主任</td>
                                        <td>学生生活管理者</td>

                                        <td class="text-center">
                                            <a href="/role/findById.do?id=${role.id}" class="btn bg-olive btn-xs">详情</a>
                                            <a href="${pageContext.request.contextPath}/pages/system/role/role-permission-add.jsp"
                                               class="btn bg-olive btn-xs">更改权限资源</a>
                                        </td>
                                    </tr>--%>
                            <c:forEach items="${page.list}" var="role" varStatus="s">
                                <tr>
                                    <td><input name="id" type="checkbox" value="${role.id}"></td>
                                    <td>${s.count}</td>
                                    <td>${role.roleName }</td>
                                    <td>${role.roleDesc }</td>
                                    <td class="text-center">
                                        <button type="button" class="btn bg-olive btn-xs"
                                                onclick='location.href="/system/role?operation=toUpdate&id=${role.id}"'>
                                            编辑
                                        </button>
                                        <button type="button" class="btn bg-olive btn-xs"
                                                onclick='location.href="/system/role?operation=author&id=${role.id}"'>
                                            权限设置
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </form>
                    <!--数据列表/-->
                </div>
                <!-- 数据表格 /-->
            </div>

            <div class="box-footer">
                <jsp:include page="../../common/page.jsp">
                    <jsp:param value="${pageContext.request.contextPath}/system/role?operation=list"
                               name="pageUrl"/>
                </jsp:include>
            </div>


        </div>
    </section>
    <!-- 正文区域 /-->
</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document)
        .ready(
            function () {

                // 激活导航位置
                setSidebarActive("admin-datalist");

                // 列表按钮
                $("#dataList td input[type='checkbox']")
                    .iCheck(
                        {
                            checkboxClass: 'icheckbox_square-blue',
                            increaseArea: '20%'
                        });
                // 全选操作
                $("#selall")
                    .click(
                        function () {
                            var clicks = $(this).is(
                                ':checked');
                            if (!clicks) {
                                $(
                                    "#dataList td input[type='checkbox']")
                                    .iCheck(
                                        "uncheck");
                            } else {
                                $(
                                    "#dataList td input[type='checkbox']")
                                    .iCheck("check");
                            }
                            $(this).data("clicks",
                                !clicks);
                        });
            });
</script>
</body>

</html>