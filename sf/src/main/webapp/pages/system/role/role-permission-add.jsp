<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="../../base.jsp" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>
<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">

    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            角色管理
            <small>添加权限表单</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="${pageContext.request.contextPath}/index.jsp">
                    <i class="fa fa-dashboard"></i> 首页
                </a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/role/findAll.do">角色管理</a>
            </li>
            <li class="active">添加权限表单</li>
        </ol>
    </section>
    <!-- 内容头部 /-->
    <form action="${pageContext.request.contextPath}/role/addPermissionToRole.do" method="post">
        <!-- 正文区域 -->
        <section class="content">

            <input type="hidden" name="roleId" value="${role.id}">
            <table id="dataList" class="table table-bordered table-striped table-hover dataTable">
                <thead>
                <tr>
                    <th class="" style="padding-right: 0px">
                        <input id="selall"
                               type="checkbox" class="icheckbox_square-blue">
                    </th>
                    <th class="sorting_asc">ID</th>
                    <th class="sorting">权限名称</th>
                    <th class="sorting">权限URL</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <input name="ids" type="checkbox" value="${permission.id}">
                    </td>
                    <td>1</td>
                    <td>findAll</td>
                    <td>/user/findAll</td>
                </tr>
                <%--<c:forEach items="${permissionList}" var="permission">
                    <tr>
                        <td>
                            <input name="ids" type="checkbox" value="${permission.id}">
                        </td>
                        <td>${permission.id}</td>
                        <td>${permission.permissionName }</td>
                        <td>${permission.url}</td>
                    </tr>
                </c:forEach>--%>
                </tbody>
            </table>
            <!--订单信息/--> <!--工具栏-->
            <div class="box-tools text-center">
                <button type="submit" class="btn bg-maroon">保存</button>
                <button type="button" class="btn bg-default"
                        onclick="history.back(-1);">返回
                </button>
            </div>
            <!--工具栏/-->
        </section>
        <!-- 正文区域 /-->
    </form>
</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }
</script>
</body>
</html>