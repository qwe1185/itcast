<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../../base.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>

<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">

    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            日志管理
            <small>全部日志</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="${pageContext.request.contextPath}/index.jsp"><i
                    class="fa fa-dashboard"></i> 首页</a></li>
            <li><a
                    href="${pageContext.request.contextPath}/sysLog/findAll.do">日志管理</a></li>

            <li class="active">全部日志</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content"> <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">列表</h3>
            </div>

            <div class="box-body">

                <!-- 数据表格 -->
                <div class="table-box">

                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick="window.location.reload();">
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick=deleteCheck()>
                                    <i class="fa fa-trash"></i> 删除
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick=location.href="/system/syslog?operation=downloadReport">
                                    <i class="fa fa-arrow-up"></i> 导出
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm"
                                   placeholder="搜索"> <span
                                class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                    <!--工具栏/-->

                    <!--数据列表-->
                    <table id="dataList"
                           class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th class="" style="padding-right: 0px"><input id="selall"
                                                                           type="checkbox"
                                                                           class="icheckbox_square-blue"></th>
                            <th class="sorting_asc">序号</th>
                            <th class="sorting">访问时间</th>
                            <th class="sorting">访问用户</th>
                            <th class="sorting">访问IP</th>
                            <th class="sorting">资源URL</th>
                            <th class="sorting">执行时间</th>
                            <th class="sorting">访问方法</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${page.list}" var="syslog" varStatus="count">
                            <tr>
                                <td><input name="ids" type="checkbox" value="${syslog.id}"></td>
                                <td>${count.count}</td>
                                <td>${syslog.visitTimeStr }</td>
                                <td>${syslog.username }</td>
                                <td>${syslog.ip }</td>
                                <td>${syslog.url}</td>
                                <td>${syslog.executionTime}毫秒</td>
                                <td>${syslog.method}</td>
                            </tr>
                        </c:forEach>
                        </tbody>

                    </table>
                    <!--数据列表/-->

                </div>
                <!-- 数据表格 /-->

            </div>
            <!-- /.box-body -->

            <!-- .box-footer-->
            <div class="box-footer">
                <jsp:include page="../../common/page.jsp">
                    <jsp:param value="${pageContext.request.contextPath}/system/syslog?operation=list"
                               name="pageUrl"/>
                </jsp:include>
            </div>
            <!-- /.box-footer-->

        </div>

    </section>
    <!-- 正文区域 /-->

</div>
<!-- 内容区域 /-->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    function deleteCheck() {
        var chedk = document.getElementsByName("ids");
        var arr = new Array();
        var flag = false;

        for (var i = 0; i < chedk.length; i++) {
            if (chedk[i].checked) {
                arr.push(chedk[i].value);
                flag = true;
            }
        }

        //1.1 判断flag
        if (!flag) {
            // 一个都没勾选
            alert("请勾选您要删除的数据!!!");
            return false;
        }

        location.href = "${pageContext.request.contextPath}/system/syslog?operation=delete&ids=" + arr;

    }


    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {

        // 激活导航位置
        setSidebarActive("order-manage");

        // 列表按钮
        $("#dataList td input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });
    });
</script>
</body>

</html>