<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@include file="../../base.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>数据 - AdminLTE2定制版</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">

    <link href="${pageContext.request.contextPath}/validate/jquery.validator.css" rel="stylesheet"/>
    <script src="${pageContext.request.contextPath}/validate/jquery-1.11.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/validate/jquery.validator.min.js"></script>
    <script src="${pageContext.request.contextPath}/validate/local/zh-CN.js"></script>
</head>
<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            用户管理
            <small>用户表单</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="${pageContext.request.contextPath}/index.jsp">
                    <i class="fa fa-dashboard"></i> 首页
                </a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/user/findAll.do">用户管理</a>
            </li>
            <li class="active">用户表单</li>
        </ol>
    </section>
    <!-- 内容头部 /-->
    <form action="${pageContext.request.contextPath}/system/user?operation=save"
          method="post" id="userAddForm">
        <!-- 正文区域 -->
        <section class="content"> <!--产品信息-->
            <div class="panel panel-default">
                <div class="panel-heading">用户信息</div>
                <div class="row data-type">
                    <div class="col-md-2 title">用户名称</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="username" placeholder="用户名称" value="">
                    </div>
                    <div class="col-md-2 title">密码</div>
                    <div class="col-md-4 data">
                        <input type="password" class="form-control" name="password" placeholder="密码" value="">
                    </div>
                    <div class="col-md-2 title">邮箱</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="email" placeholder="邮箱" value="" id="email">
                    </div>
                    <div class="col-md-2 title">联系电话</div>
                    <div class="col-md-4 data">
                        <input type="text" class="form-control" name="phoneNum" placeholder="联系电话" value="">
                    </div>
                    <div class="col-md-2 title">用户状态</div>
                    <div class="col-md-4 data">
                        <select class="form-control select2" style="width: 100%" name="status">
                            <option value="0" selected="selected">关闭</option>
                            <option value="1">开启</option>
                        </select>
                    </div>
                </div>
            </div>
            <!--订单信息/--> <!--工具栏-->
            <div class="box-tools text-center">
                <button type="submit" class="btn bg-maroon">保存</button>
                <button type="button" class="btn bg-default"
                        onclick="history.back(-1);">返回
                </button>
            </div>
            <!--工具栏/-->
        </section>
        <!-- 正文区域 /-->
    </form>
</div>
<!-- 内容区域 /-->
<script>
    var res = true;
    $("#email").blur(function () {
        //2. 获取输入的邮箱
        var email = this.value;  // this 表示当前操作的JS对象
        if (email != "") {
            //3. 发生异步请求去服务器校验
            $.post("/system/user?operation=emailCheck", {"email": email}, function (data) {
                if (data === "true") {
                    res = true;
                } else {
                    alert("邮箱已注册!~");
                    res = false;
                }
                /*if (data === "false") {

                }*/
            }, "text");
        }
    });
    $("#userAddForm").submit(function () {
        if (res) {
            $("#userAddForm").submit();
        } else {
            return res;
        }
    });

    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }
</script>


</body>

</html>