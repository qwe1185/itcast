<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="../../base.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>用户管理页面</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>

<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">

    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            用户管理
            <small>全部用户</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="/index.jsp">
                    <i class="fa fa-dashboard"></i>
                    首页
                </a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/system/user?operation=list">用户管理</a>
            </li>
            <li class="active">全部用户</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content"> <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">列表</h3>
            </div>

            <div class="box-body">
                <!-- 数据表格 -->
                <div class="table-box">
                    <!--工具栏-->
                    <div class="pull-left">
                        <div class="form-group form-inline">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default" title="新建"
                                        onclick="location.href='${pageContext.request.contextPath}/system/user?operation=toAdd'">
                                    <i class="fa fa-file-o"></i> 新建
                                </button>
                                <button type="button" class="btn btn-default" title="删除" onclick="deleteByIds();">
                                    <i class="fa fa-trash"></i> 删除
                                </button>
                                <button type="button" class="btn btn-default" title="刷新"
                                        onclick='location.href="${pageContext.request.contextPath}/system/user?operation=list"'>
                                    <i class="fa fa-refresh"></i> 刷新
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <form action="${pageContext.request.contextPath}/system/user?operation=findByName"
                                  method="post" id="findForm">
                                <input type="text" placeholder="搜索" id="username" name="desc" value="${desc}">
                                <input type="button" value="搜索" onclick="findByName();">
                            </form>
                        </div>
                    </div>
                    <!--工具栏/-->

                    <!--数据列表-->
                    <form method="post" id="form"
                          action="${pageContext.request.contextPath}/system/user?operation=delete">
                        <table id="dataList"
                               class="table table-bordered table-striped table-hover dataTable">
                            <thead>
                            <tr>
                                <th class="" style="padding-right: 0px">
                                    <input id="selall" type="checkbox" class="icheckbox_square-blue">
                                </th>
                                <th class="sorting_asc">序号</th>
                                <th class="sorting_desc">用户名</th>
                                <th class="sorting_asc sorting_asc_disabled">邮箱</th>
                                <th class="sorting_desc sorting_desc_disabled">联系电话</th>
                                <th class="sorting">状态</th>
                                <th class="text-center">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${page.list}" var="item" varStatus="status">
                                <tr>
                                    <td><input name="ids" id="ids" type="checkbox" value="${item.id}"></td>
                                    <td>${status.count}</td>
                                    <td>${item.username}</td>
                                    <td>${item.email}</td>
                                    <td>${item.phoneNum}</td>
                                    <td>
                                        <button type="button" class="btn btn-default" title="更改状态"
                                                onclick="location.href='${pageContext.request.contextPath}/system/user?operation=updateStatus&id=${item.id}'">
                                                ${item.statusStr}
                                        </button>
                                    </td>
                                    <td class="text-center">
                                        <a href="${pageContext.request.contextPath}/system/user?operation=toShow&uid=${item.id}"
                                           class="btn bg-olive btn-xs">详情</a>
                                        <a href="${pageContext.request.contextPath}/system/user?operation=toRole&uid=${item.id}"
                                           class="btn bg-olive btn-xs">更改角色</a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <div class="box-footer">
                <jsp:include page="../../common/page.jsp">
                    <jsp:param
                            value="${pageContext.request.contextPath}${empty desc ? '/system/user?operation=list' : '/system/user?operation=findByName'}"
                            name="pageUrl"/>
                </jsp:include>
            </div>
        </div>
    </section>
    <!-- 正文区域 /-->
</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    function findByName() {
        var username = $("#username").val();

        //判断用户名是否为空
        if (username == null || username == "") {
            location.href = "${pageContext.request.contextPath}/system/user?operation=list";
            return;
        }
        //如果不为空
        document.getElementById("findForm").submit();
    }

    //实现获取复选框ID，实现删除功能
    function deleteByIds() {
        //1. 判断是否至少勾选了一个用户
        var chks = document.getElementsByName("ids");
        var flag = false;
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked == true) {
                flag = true;
            }
        }

        //1.1 判断flag
        if (!flag) {
            // 一个都没勾选
            alert("请勾选您要删除的数据!!!");
            return false;
        }


        //2. 删除询问
        if (confirm("您确定要删除选中么?")) {
            // 提交表单
            document.getElementById("form").submit();
        }
        /*//获取复选对象
        var ids = document.getElementsByName("ids");
        var arr = new Array();
        //alert("ok:"+ids.length);
        for (var i = 0; i < ids.length; i++) {
            //判断出被选中的复选框
            if (ids[i].checked) {//true
               alert(ids[i].value);
                //arr[i]=ids[i].value;
                arr.push(ids[i].value)
            }
        }
        //alert("ok:"+arr);
        //访问controller
        */
    }


    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {
        // 激活导航位置
        setSidebarActive("admin-datalist");

        // 列表按钮
        $("#dataList td input[type='checkbox']")
            .iCheck(
                {
                    checkboxClass: 'icheckbox_square-blue',
                    increaseArea: '20%'
                });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(
                ':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });
    });
</script>
</body>

</html>