<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="../../base.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>用户详情页</title>
    <meta name="description" content="AdminLTE2定制版">
    <meta name="keywords" content="AdminLTE2定制版">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
</head>

<body>
<div id="frameContent" class="content-wrapper" style="margin-left:0px;">
    <!-- 内容头部 -->
    <section class="content-header">
        <h1>
            用户管理
            <small>全部用户</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="${pageContext.request.contextPath}/index.jsp">
                    <i class="fa fa-dashboard"></i> 首页
                </a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/system/user?operation=list">用户管理</a>
            </li>
            <li class="active">全部用户</li>
        </ol>
    </section>
    <!-- 内容头部 /-->

    <!-- 正文区域 -->
    <section class="content"> <!-- .box-body -->
        <div class="box box-primary">
            <div class="box-body">
                <!-- 数据表格 -->
                <div class="table-box">
                    <!--数据列表-->
                    <div><h3>用户角色信息</h3>
                        <div/>
                        <div class="tab-pane" id="tab-treetable">
                            <table id="collapse-table" class="table table-bordered table-hover dataTable">
                                <thead>
                                <tr>
                                    <th>角色名称</th>
                                    <th>角色描述</th>
                                </tr>
                                </thead>

                                <%--<tr data-tt-id="0">
                                    <td colspan="2">${user.username}</td>
                                </tr>--%>

                                <tbody>
                                <c:forEach items="${user.roles}" var="role">
                                    <tr data-tt-id="1" data-tt-parent-id="0">
                                        <td>${role.roleName }</td>
                                        <td>${role.roleDesc }</td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!--数据列表/-->

                        <br>
                        <br>
                        <br>
                        <div><h3>用户详细信息</h3>
                            <div/>
                            <div class="row data-type">

                                <div class="col-md-2 title">用户名称</div>
                                <div class="col-md-4 data">
                                    <input type="text" class="form-control" name="username"
                                           placeholder="用户名称" value="${user.username}" disabled="disabled">
                                </div>
                                <%--<div class="col-md-2 title">密码</div>
                                <div class="col-md-4 data">
                                    <input type="text" class="form-control" name="password"
                                           placeholder="密码" value="${user.password}">
                                </div>--%>
                                <div class="col-md-2 title">邮箱</div>
                                <div class="col-md-4 data">
                                    <input type="text" class="form-control" name="email"
                                           placeholder="邮箱" value="${user.email}" disabled="disabled">
                                </div>
                                <div class="col-md-2 title">联系电话</div>
                                <div class="col-md-4 data">
                                    <input type="text" class="form-control" name="phoneNum"
                                           placeholder="联系电话" value="${user.phoneNum}" disabled="disabled">
                                </div>
                                <div class="col-md-2 title">用户状态</div>
                                <div class="col-md-4 data">
                                    <input type="text" class="form-control" name="phoneNum"
                                           placeholder="联系电话" value="${user.status == '0' ? "关闭" : "开启"}"
                                           disabled="disabled">
                                </div>

                            </div>
                            <!-- 数据表格 /-->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="button" class="btn bg-default" style="width: 100%"
                                    onclick="history.back(-1);">返回
                            </button>
                        </div>
                    </div>
    </section>
    <!-- 正文区域 /-->
</div>
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<script>
    $(document).ready(function () {
        // 选择框
        $(".select2").select2();

        // WYSIHTML5编辑器
        $(".textarea").wysihtml5({
            locale: 'zh-CN'
        });
        $("#collapse-table").treetable({
            expandable: true
        });
    });

    // 设置激活菜单
    function setSidebarActive(tagUri) {
        var liObj = $("#" + tagUri);
        if (liObj.length > 0) {
            liObj.parent().parent().addClass("active");
            liObj.addClass("active");
        }
    }

    $(document).ready(function () {

        // 激活导航位置
        setSidebarActive("admin-datalist");

        // 列表按钮
        $("#dataList td input[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });
        // 全选操作
        $("#selall").click(function () {
            var clicks = $(this).is(':checked');
            if (!clicks) {
                $("#dataList td input[type='checkbox']").iCheck("uncheck");
            } else {
                $("#dataList td input[type='checkbox']").iCheck("check");
            }
            $(this).data("clicks", !clicks);
        });
    });
</script>
</body>

</html>