package com.itheima.service.system;

import com.github.pagehelper.PageInfo;
import com.itheima.domain.system.Role;
import com.itheima.service.system.impl.RoleServiceImpl;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class RoleServiceTest {
    private static RoleService roleService = null;
    @BeforeClass
    public static void init(){
        roleService = new RoleServiceImpl();
    }

    @Test
    public void testSave(){
        Role role = new Role();
        role.setRoleName("测试数据");
        roleService.save(role);
    }

    @Test
    public void testFindAll(){
        PageInfo all = roleService.findAll(1, 100);
        System.out.println(all);
    }

    @AfterClass
    public static void destory(){
        roleService = null;
    }


}
